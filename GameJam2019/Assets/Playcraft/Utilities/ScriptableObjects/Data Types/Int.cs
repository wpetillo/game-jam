﻿using UnityEngine;

[CreateAssetMenu(menuName = "VRKit/Basic Data Types/Int")]
public class Int : ScriptableObject
{
    public int Value;
}