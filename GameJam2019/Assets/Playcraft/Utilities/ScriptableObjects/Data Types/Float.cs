﻿using UnityEngine;

[CreateAssetMenu(menuName = "VRKit/Basic Data Types/Float")]
public class Float : ScriptableObject
{
    public float Value;
}
