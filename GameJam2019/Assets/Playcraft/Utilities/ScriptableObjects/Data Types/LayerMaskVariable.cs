﻿using System;
using UnityEngine;

[CreateAssetMenu]
public class LayerMaskVariable : ScriptableObject, ISerializationCallbackReceiver
{
    public LayerMask InitialValue;

    public LayerMask RuntimeValue;

    public void OnAfterDeserialize() 
    {
        RuntimeValue = InitialValue;
    }

    public void OnBeforeSerialize() { }
}