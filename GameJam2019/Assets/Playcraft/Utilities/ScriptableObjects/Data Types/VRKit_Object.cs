﻿using UnityEngine;

[CreateAssetMenu(menuName = "VRKit/Basic Data Types/Object")]
public class VRKit_Object : ScriptableObject
{
    public GameObject Asset;
    [HideInInspector] public GameObject Instance;
}
