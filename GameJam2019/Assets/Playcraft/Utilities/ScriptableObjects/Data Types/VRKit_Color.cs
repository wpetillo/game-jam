﻿using UnityEngine;

[CreateAssetMenu(menuName = "VRKit/Basic Data Types/Color")]
public class VRKit_Color : ScriptableObject
{
    public Color Value;
}
