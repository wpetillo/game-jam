﻿using UnityEngine;

public class ObjectSceneToAsset : MonoBehaviour
{
    [SerializeField] VRKit_Object vrkObject;
    [SerializeField] bool deactivateOnAwake;

    private void Awake()
    {
        vrkObject.Instance = gameObject;

        if (deactivateOnAwake)
            gameObject.SetActive(false);
    }
}
