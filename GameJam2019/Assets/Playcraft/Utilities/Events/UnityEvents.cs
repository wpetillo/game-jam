﻿using System;
using UnityEngine;
using UnityEngine.Events;

// Primitives
[Serializable] public class IntEvent : UnityEvent<int> { }
[Serializable] public class FloatEvent : UnityEvent<float> { }
[Serializable] public class Vector2Event : UnityEvent<Vector2> { }
[Serializable] public class Vector3Event : UnityEvent<Vector3> { }
[Serializable] public class Vector3x2Event : UnityEvent<Vector3, Vector3> { }
[Serializable] public class LayerEvent : UnityEvent<LayerMask> { }

// Custom classes
[Serializable] public class MoveCommandEvent : UnityEvent<MoveCommand> { }

