﻿using UnityEngine;

[CreateAssetMenu(fileName = "Game Event", menuName = "Playcraft/Events/MoveCommand")]
public class MoveCommandGameEvent : GameEvent
{
    public void Raise(MoveCommand value)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
            listeners[i].OnEventRaised(value);
    }
}