﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Game Event", menuName = "Playcraft/Events/GameObject")]
public class GameObjectEvent : GameEvent
{
    public void Raise(GameObject value)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
            listeners[i].OnEventRaised(value);
    }
}
