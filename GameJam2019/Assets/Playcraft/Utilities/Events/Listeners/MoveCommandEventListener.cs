﻿public class MoveCommandEventListener : GameEventListener
{
    public MoveCommandEvent MoveCommandResponse;

    public override void OnEventRaised(MoveCommand value)
    { MoveCommandResponse.Invoke(value); }
}
