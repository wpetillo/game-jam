﻿using UnityEngine;
using UnityEngine.Events;

public class LayerEventListener : GameEventListener
{
    public LayerEvent Response;

    public override void OnEventRaised(LayerMask value)
    { Response.Invoke(value); }
}
