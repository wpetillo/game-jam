﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectEventListener : GameEventListener
{
    public GameObjectEvent gameObjectResponse;

    public override void OnEventRaised(GameObject value)
    { gameObjectResponse.Raise(value); }
}
