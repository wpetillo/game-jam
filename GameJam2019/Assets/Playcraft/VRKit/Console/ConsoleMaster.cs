﻿using UnityEngine;
using UnityEngine.XR;

public class ConsoleMaster : MonoBehaviour
{
    [SerializeField] ConsoleSettings VRSettings, FlatSettings, AsymSettings;
    public ConsoleSettings gameVRSetting;
    [SerializeField] bool autoDetectConsole;
    [SerializeField] GameObject flatscreenCamera, viveRig, steamVR;
    [SerializeField] AudioListener viveAudio;

    void Start()
    {
        CheckDeviceConnections();  
        ApplyDeviceSettings();     
    }

    // Convert Automatic mode to VR or Flatscreen
    // * Upgrading to SteamVR version 2 may make this method obsolete
    public void CheckDeviceConnections()
    {
        if (autoDetectConsole)
        {
            string VRmodel = "none";

            if (UnityEngine.XR.XRSettings.enabled)
                if (UnityEngine.XR.XRDevice.model != null)
                    VRmodel = UnityEngine.XR.XRDevice.model;
            
            // TBD: adapt to different VR devices
            // Debug.Log("VR device enabled: " + VRmodel);   

            if (VRmodel == "none")
                gameVRSetting = FlatSettings;
            else
                gameVRSetting = VRSettings;
        }
    }

    // Activate/deactivate objects based on mode
    private void ApplyDeviceSettings()
    {
        UnityEngine.XR.XRSettings.showDeviceView = gameVRSetting.showVrDeviceView;
        flatscreenCamera.SetActive(!gameVRSetting.showVrDeviceView);
        viveAudio.enabled = gameVRSetting.useViveAudio;
        viveRig.SetActive(gameVRSetting.isViveRigActive);
        steamVR.SetActive(gameVRSetting.isViveRigActive);
    }

    public void LoadLevel()
    {
        CheckDeviceConnections();  
        ApplyDeviceSettings();     
    }
}