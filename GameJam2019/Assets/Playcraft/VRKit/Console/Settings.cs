﻿using UnityEngine;

[CreateAssetMenu(fileName = "Device Settings", menuName = "Playcraft/VR Kit/Device Settings")]
public class ConsoleSettings : ScriptableObject
{
    public bool usingFlatscreenKeyboard;
    public bool isViveRigActive;
    public bool showVrDeviceView;
    public bool useViveAudio;
}
