﻿using UnityEngine;

public class GrabTrigger : MonoBehaviour, IGrabbable
{
    [SerializeField] GameObject observer;
    IRecieveTarget cachedObserver;

    private void Awake()
    {
        cachedObserver = observer.GetComponent<IRecieveTarget>();
    }

    public GameObject GetObject()
    {
        return gameObject;
    }

    public virtual void OnGrab(Transform hand)
    {
        cachedObserver.SetTarget(hand);
    }

    public void OnRelease(Transform hand, Vector3 velocity, Vector3 angularVelocity)
    {
        cachedObserver.RemoveTarget();
    }
}
