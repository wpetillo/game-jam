﻿using UnityEngine;

public class Throwable : Pickup
{
    [SerializeField] float throwForce = 1;

    // Set velocity based on controller movement
    public override void OnRelease(Transform hand, Vector3 velocity, Vector3 angularVelocity)
    {
        base.OnRelease(hand, velocity, angularVelocity);

        rb.velocity = velocity * throwForce;
        rb.angularVelocity = angularVelocity;
    }
}
