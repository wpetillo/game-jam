﻿using UnityEngine;

public class Pickup : MonoBehaviour, IGrabbable
{
    [SerializeField] VRKit_Object assignedParent;
    Transform defaultParent;

    [SerializeField] protected Rigidbody rb;
    [SerializeField] int grabHaptics;

    bool kinematicDefault;
    bool gravityDefault;
    bool hasInitialized = false;

    protected virtual void Initialize()
    {
        kinematicDefault = rb.isKinematic;
        gravityDefault = rb.useGravity;
        hasInitialized = true;

        if (assignedParent == null)
            defaultParent = transform.parent;
        else
            defaultParent = assignedParent.Instance.transform;

        rb.transform.parent = transform.parent;
    }

    public GameObject GetObject()
    {
        return gameObject;
    }

    public virtual void OnGrab(Transform hand)
    {
        if (!hasInitialized)
            Initialize();

        rb.transform.parent = hand;
        rb.isKinematic = true;
        rb.useGravity = false;

        //MainGameManager.instance.SendHapticSignal(hand, grabHaptics);
    }

    public virtual void OnRelease(Transform hand, Vector3 velocity, Vector3 angularVelocity)
    {
        if (rb.transform.parent == hand.transform)
            rb.transform.parent = defaultParent;

        rb.isKinematic = kinematicDefault;
        rb.useGravity = gravityDefault;
    }
}
