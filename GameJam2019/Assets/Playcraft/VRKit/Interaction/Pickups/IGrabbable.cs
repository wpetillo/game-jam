﻿using UnityEngine;

public interface IGrabbable
{
    void OnGrab(Transform hand);
    void OnRelease(Transform hand, Vector3 velocity, Vector3 angularVelocity);
    GameObject GetObject();
}
