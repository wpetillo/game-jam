﻿using UnityEngine;

public class SpawnerHand : Hand
{
    //[SerializeField] ObjectPoolMaster spawner;
    [SerializeField] GameObject[] prefabs;
    [SerializeField] Collider validSpawnArea;
    [SerializeField] CycleMenu menu;
    ObjectPoolMaster spawner;

    private void Start()
    {
        spawner = ObjectPoolMaster.instance;
    }

    // Changed From public
    protected override void Grab()
    {
        if (validSpawnArea == null)
            SpawnCurrentObject();

        Collider[] touching = Physics.OverlapSphere(transform.position, .1f);

        foreach (Collider collider in touching)
        {
            if (collider == validSpawnArea)
            {
                SpawnCurrentObject();
                break;
            }
        }

        base.Grab();
    }

    private void SpawnCurrentObject()
    {
        spawner.Spawn(prefabs[menu.currentIndex], transform.position);
    }
}
