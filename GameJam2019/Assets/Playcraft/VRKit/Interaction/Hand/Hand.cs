﻿using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class Hand : MonoBehaviour
{
    [SerializeField] public SteamVR_Input_Sources thisHand;
    public SteamVR_Action_Boolean grabPinch;
    [SteamVR_DefaultAction("Haptic")]
    public SteamVR_Action_Vibration hapticAction;
    [SerializeField] Animator animator;

    [SerializeField] float baseGrabRadius;
    protected float currentGrabRadius;
    protected float startScale;
    protected float currentScale;

    protected Rigidbody rb;
    protected IGrabbable itemInGrasp;
    protected Collider[] collidersInGrasp;

    protected virtual void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        startScale = transform.localScale.x;
        currentScale = startScale;
        AdjustToRigScale(currentScale);
    }

    protected virtual void OnEnable()
    {
        if (grabPinch != null)
            grabPinch.AddOnChangeListener(OnTriggerPressedOrReleased, thisHand);
    }

    protected virtual void OnDisable()
    {
        if (grabPinch != null)
            grabPinch.RemoveOnChangeListener(OnTriggerPressedOrReleased, thisHand);
    }

    private void OnTriggerPressedOrReleased(SteamVR_Action_In actionIn)
    {
        if (grabPinch.GetStateDown(thisHand))
        {
            animator.SetBool("Closed", true);
            Grab();
        }
        else
        {
            animator.SetBool("Closed", false);
            Release(rb.velocity, rb.angularVelocity);
        }
    }

    public void AdjustToRigScale(float rigScale)
    {
        currentScale = rigScale;
        currentGrabRadius = baseGrabRadius * rigScale;
        transform.localScale = new Vector3(currentScale, currentScale, currentScale);
        //Debug.Log("grab radius" + currentGrabRadius);
    }

    protected virtual void Grab()
    {
        List<IGrabbable> grabbedItems = new List<IGrabbable>();

        collidersInGrasp = null;
        collidersInGrasp = Physics.OverlapSphere(transform.position, currentGrabRadius);

        foreach (Collider collider in collidersInGrasp)
            if (collider.gameObject.GetComponent<IGrabbable>() != null)
                grabbedItems.Add(collider.gameObject.GetComponent<IGrabbable>());

        if (grabbedItems.Count == 0)
            itemInGrasp = null;
        else
        {
            itemInGrasp = ClosestToCenter(grabbedItems);
            itemInGrasp.OnGrab(transform);
        }
    }

    IGrabbable ClosestToCenter(List<IGrabbable> grabbedItems)
    {
        IGrabbable itemClosestToCenter = grabbedItems[0];
        float centerToClosest = Mathf.Infinity;

        foreach (IGrabbable item in grabbedItems)
        {
            float centerToCurrent = Vector3.Distance(item.GetObject().transform.position, transform.position);

            if (centerToCurrent < centerToClosest)
            {
                itemClosestToCenter = item;
                centerToClosest = centerToCurrent;
            }
        }

        return itemClosestToCenter;
    }

    protected virtual void Release(Vector3 velocity, Vector3 angularVelocity)
    {
        if (itemInGrasp != null)
            itemInGrasp.OnRelease(transform, velocity, angularVelocity);
    }
}
