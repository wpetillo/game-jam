﻿using UnityEngine;

public class Hinge : MonoBehaviour, IRecieveTarget
{
    [SerializeField] Transform referenceAngle;
    [SerializeField] float minRotation, maxRotation;
    [SerializeField] float rotationSpeed;
    Transform target;

    Vector3 targetDirection;
    Vector3 closeAngle, openAngle;
    float targetAngle;

    private void Start()
    {
        closeAngle = new Vector3(minRotation, 0f, 0f);
        openAngle = new Vector3(maxRotation, 0f, 0f);
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }

    public void RemoveTarget()
    {
        target = null;
    }

    private void FixedUpdate()
    {
        if (target == null)
            Close();
        else
            FollowTarget();
    }

    private void RotateStep()
    {
        transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(targetDirection), rotationSpeed * Time.deltaTime);
    }

    private void FollowTarget()
    {
        SetTargetAngle();
        ApplyClamp();
        RotateStep();
    }

    private void SetTargetAngle()
    {
        targetDirection = target.position - referenceAngle.position;
        targetAngle = Vector3.SignedAngle(referenceAngle.forward, targetDirection, Vector3.right);
    }

    private void ApplyClamp()
    {
        if (targetAngle < minRotation)
            targetAngle = minRotation;
        else if (targetAngle > maxRotation)
            targetAngle = maxRotation;

        targetDirection = new Vector3(targetAngle, 0f, 0f);
    }

    private void Close()
    {
        if (transform.eulerAngles.x == minRotation)
            return;

        targetDirection = closeAngle;
        RotateStep();
    }

    private void Open()
    {
        if (transform.eulerAngles.x == maxRotation)
            return;

        targetDirection = openAngle;
        RotateStep();
    }
}
