﻿using UnityEngine;

public interface IRecieveTarget
{
    void SetTarget(Transform target);
    void RemoveTarget();
}
