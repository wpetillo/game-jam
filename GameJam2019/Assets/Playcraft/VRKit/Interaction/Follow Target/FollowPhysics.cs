﻿using UnityEngine;

public class FollowPhysics : MonoBehaviour, IRecieveTarget
{
    [SerializeField] Transform target;
    Rigidbody targetRigidbody;

    public float moveSpeed = 10f;
    public float spinSpeed = 0.5f;

    Quaternion angleDifference;
    Quaternion correction;
    Vector3 perpendicular;
    Vector3 mainRotation;
    Vector3 correctiveRotation;
    float angleToCorrect;

    void Start()
    {
        if (target != null)
            targetRigidbody = target.gameObject.GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (target == null)
            return;

        AlignPosition();
        AlignRotation();
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
        targetRigidbody = this.target.gameObject.GetComponent<Rigidbody>();
    }

    public void RemoveTarget()
    {
        this.target = null;
        targetRigidbody = null;
    }

    void AlignPosition()
    {
        targetRigidbody.AddForce(((transform.position - target.position) * moveSpeed) - targetRigidbody.velocity, ForceMode.VelocityChange);
    }

    void AlignRotation()
    {
        angleDifference = Quaternion.FromToRotation(target.up, transform.up);
        angleToCorrect = Quaternion.Angle(transform.rotation, target.rotation);
        perpendicular = Vector3.Cross(transform.up, transform.forward);

        if (Vector3.Dot(target.forward, perpendicular) < 0)
            angleToCorrect *= -1;

        correction = Quaternion.AngleAxis(angleToCorrect, transform.up);
        mainRotation = RectifyAngleDifference((angleDifference).eulerAngles);
        correctiveRotation = RectifyAngleDifference((correction).eulerAngles);
        targetRigidbody.AddTorque((mainRotation - correctiveRotation * spinSpeed) - targetRigidbody.angularVelocity, ForceMode.VelocityChange);
    }

    Vector3 RectifyAngleDifference(Vector3 angle)
    {
        if (angle.x > 180) angle.x -= 360;
        if (angle.y > 180) angle.y -= 360;
        if (angle.z > 180) angle.z -= 360;

        return angle;
    }
}
