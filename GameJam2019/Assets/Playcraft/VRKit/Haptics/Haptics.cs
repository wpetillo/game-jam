﻿using System.Collections;
using UnityEngine;

public class Haptics : MonoBehaviour
{
    private ControllerInput controller;
    //private SteamVR_TrackedObject trackedObject;
    //private SteamVR_Controller.Device device;

    public LayerMask HapticLayer;
    private LayerUtility layerUtility = new LayerUtility();

    const int INTENSITY = 2000;
    private bool canFire;

    private void Start()
    {
        controller = GetComponent<ControllerInput>();
        //trackedObject = controller.trackedObject;
    }

    public void EventHaptics(int duration)
    {
        if (duration == 1)
            VibrateShort(INTENSITY);
        else if (duration > 1)
            StartCoroutine(VibrateLong(INTENSITY, duration));
    }

    private void OnTriggerEnter(Collider other)
    {
        RequestHapticFeedback(other.gameObject);
    }

    private void OnCollisionEnter(Collision other)
    {
        RequestHapticFeedback(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        canFire = true;
    }

    private void OnCollisionExit(Collision other)
    {
        canFire = true;
    }

    private void RequestHapticFeedback(GameObject other)
    {
        if (!canFire) return;
        else canFire = false;

        if (layerUtility.IsIndexInBinary(other.layer, HapticLayer.value))
        {
            var haptics = other.GetComponent<HapticEffectData>();

            if (haptics == null)
                VibrateShort(INTENSITY);
            else
                StartCoroutine(VibrateLong(INTENSITY, haptics.hapticPulseDuration.Value));
        }
    }

    private void VibrateShort(ushort amt)
    {
        //device = SteamVR_Controller.Input((int)trackedObject.index);
        //device.TriggerHapticPulse(amt);
    }

    IEnumerator VibrateLong(ushort amt, int duration)
    {
        //device = SteamVR_Controller.Input((int)trackedObject.index);

        for(int i = 0; i < duration; i++)
        {
            //device.TriggerHapticPulse(amt);
            yield return new WaitForSeconds(.01f);
        }
    }
}
