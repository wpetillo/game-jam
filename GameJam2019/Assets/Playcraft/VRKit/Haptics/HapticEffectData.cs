﻿// Attach this script as a component to any object that should generate a haptic pulse on interaction
// Assign a Scriptable Object instance to set the duration of the pulse
public class HapticEffectData
{
    public Int hapticPulseDuration;
}
