﻿using UnityEngine;

// Attach to VRKit top level prefab
public class RigControl : MonoBehaviour 
{
    VRKit_Math math = new VRKit_Math();
    [SerializeField] Transform rig, leftHand, rightHand, CurrentHand;

    [SerializeField] float scaleMultiplier, maxScale, minScale;

    private bool leftGripped, rightGripped;
    private Vector3 moveCur, movePrev, moveDelta;			  	//track single hand movements
	private float distCur, distPrev, distDelta, scaleChange;	//track both hand movements
	private float scaleCur, scalePrev, scaleDelta;				//used to offset y axis when scaling to maintain perceived position

    public FloatEvent OnScaleChanged;

    public void OnGripLeft()
    {
        Debug.Log("Left hand");

        if (!leftGripped && !rightGripped)
            ResetMove();

        CurrentHand = leftHand;
        leftGripped = true;
    }

    public void OnGripRight()
    {
        Debug.Log("Right hand");

        if (!leftGripped && !rightGripped)
            ResetMove();

        CurrentHand = rightHand;
        rightGripped = true;
    }

    public void OnReleaseLeft()
    {
        Debug.Log("Left hand release");
        if (!leftGripped && !rightGripped)
            ResetMove();

        leftGripped = false;
    }

    public void OnReleaseRight()
    {
        Debug.Log("Right hand release");

        if (!leftGripped && !rightGripped)
            ResetMove();

        rightGripped = false;
    }

    void Update () 
	{
		if (leftGripped && rightGripped)
		{
			ScaleRig();
			RotateRig();
		}
		else if (leftGripped || rightGripped)
			MoveRig();
	}

	void ScaleRig()
	{
		distCur = (leftHand.position - rightHand.position).magnitude;

		if(distPrev == 0f)
			distPrev = distCur;
		else 
		{
			distDelta = (distPrev - distCur) / rig.localScale.x;
			scaleChange = 1 + (distDelta * scaleMultiplier * Time.deltaTime);

			if(scaleChange > 1.1 || scaleChange < 0.9)
				return;
			else if((scaleChange > 1 && transform.localScale.x < maxScale) || (scaleChange < 1 && rig.localScale.x > minScale))
			{
				scalePrev = rig.localScale.x;

				rig.localScale *= scaleChange;
				distPrev = distCur;

				scaleCur = rig.localScale.x;
				scaleDelta = scalePrev - scaleCur;
				rig.Translate(0, scaleDelta, 0);

                OnScaleChanged.Invoke(rig.localScale.x);
			}
		}
	}

	void MoveRig()
	{
        moveCur = GetHandPosition();

		if (movePrev == Vector3.zero)		//initialize on starting frame
			movePrev = moveCur;
		else
		{
			moveDelta = movePrev - moveCur;
            rig.position += moveDelta;
            moveCur += moveDelta;
            movePrev = moveCur;
		}
	}
    
    Vector3 GetHandPosition()
    {
        if (leftGripped && CurrentHand == leftHand || rightGripped && CurrentHand == rightHand)
            return CurrentHand.position;
        else
            return Vector3.zero;
    }

	private float angleYCur, angleYPrev, angleYChange;		// Used to rotate CameraRig
	private float angleYChangePrev, angleZChangePrev;
	private Vector3 angle3Cur;
	[SerializeField] Transform orientRotation;	
	void RotateRig()
	{
		angle3Cur = math.Span2Nodes(orientRotation, leftHand, rightHand);
		angleYCur = angle3Cur.y;

		if(angleYPrev == 0f)
			angleYPrev = angleYCur;
		else 
		{
			angleYChange = angleYPrev - angleYCur;

			if(angleYChangePrev == 0f)	
				angleYChangePrev = angleYChange;
			else if(angleYChange * angleYChangePrev < 0)    // Remove jitter by checking ignoring cases where angle change flips sign
            {
				angleYChange = 0;
				angleYChangePrev = 0;
			}

			rig.eulerAngles += new Vector3(0, angleYChange, 0);
			angleYPrev = angleYCur;
		}
	}

    private void ResetMove()
    {
        movePrev = Vector3.zero;
        distPrev = 0f;
        angleYPrev = 0f;
    }
}
