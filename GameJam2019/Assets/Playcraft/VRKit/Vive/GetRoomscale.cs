﻿using UnityEngine;
using Valve.VR;

public class GetRoomscale : MonoBehaviour 
{
	[HideInInspector] public Vector3 roomScale;

	void Start () 
	{
		var rect = new Valve.VR.HmdQuad_t();

		SteamVR_PlayArea.GetBounds(SteamVR_PlayArea.Size.Calibrated, ref rect);

		roomScale = new Vector3(Mathf.Abs(rect.vCorners0.v0 - rect.vCorners2.v0), 
			this.transform.localScale.y, Mathf.Abs(rect.vCorners0.v2 - rect.vCorners2.v2));

        if (roomScale.x < .1f)
        {
            Debug.LogWarning("Roomscale calibration failed, using default floor size...");
            roomScale = new Vector3(3f, 0.5f, 2.5f);
        }

		this.transform.localScale = roomScale;
	}
}
