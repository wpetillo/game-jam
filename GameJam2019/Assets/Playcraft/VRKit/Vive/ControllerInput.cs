﻿using UnityEngine;
using UnityEngine.Events;

// OBSOLETE: with SteamVR version 2, use only for legacy plugin
public class ControllerInput : MonoBehaviour 
{
	//public SteamVR_TrackedObject trackedObject;
	//public SteamVR_Controller.Device device;

    public UnityEvent TriggerPressDown;
    public UnityEvent TriggerPressUp;
    public UnityEvent GripPressDown;
    public UnityEvent GripPressUp;
    public Vector2Event PadPressDown;
    public Vector2Event PadPress;
    public UnityEvent PadPressUp;
    public Vector2Event PadTouchDown;
    public Vector2Event PadTouch;
    public UnityEvent PadTouchUp;
    public UnityEvent ApplicationPressDown;

    public Vector3x2Event ControllerPhysicsEvent;

    /*void Update () 
	{
        //try { device = SteamVR_Controller.Input((int)trackedObject.index); }
        //catch { return; }

        transform.SetPositionAndRotation(trackedObject.transform.position, trackedObject.transform.rotation);

        if (device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
            TriggerPressDown.Invoke();
        else if (device.GetPressUp(SteamVR_Controller.ButtonMask.Trigger))
            TriggerPressUp.Invoke();

        if (device.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
            GripPressDown.Invoke();
        else if (device.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
            GripPressUp.Invoke();

        if (device.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
            ApplicationPressDown.Invoke();

        if (device.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
            PadPressDown.Invoke(device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad));
        else if (device.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad))
            PadPressUp.Invoke();
        else if (device.GetPress(SteamVR_Controller.ButtonMask.Touchpad))
            PadPress.Invoke(device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad));

        if (device.GetTouchDown(SteamVR_Controller.ButtonMask.Touchpad))
            PadTouchDown.Invoke(device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad));
        else if (device.GetTouchUp(SteamVR_Controller.ButtonMask.Touchpad))
            PadTouchUp.Invoke();
        else if (device.GetTouch(SteamVR_Controller.ButtonMask.Touchpad))
            PadTouch.Invoke(device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad));
    }*/

    public void OnControllerPhysicsEvent()
    {
        //ControllerPhysicsEvent.Invoke(device.velocity, device.angularVelocity);
    }
}
