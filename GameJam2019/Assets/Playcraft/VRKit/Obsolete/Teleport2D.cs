﻿using UnityEngine;

public class Teleport2D : MonoBehaviour
{
    [SerializeField] GameObject rig, playerHead;

    [SerializeField] float range = 15;
    [SerializeField] LayerMask teleportMask;
    [SerializeField] GameObject aimIndicator;

    Vector3 headOffset;
    Vector3 teleportLocation;
    LineRenderer laserBeam;
    RaycastHit hit, groundRay;

    private void Awake()
    {
        laserBeam = GetComponent<LineRenderer>();
    }

    public void Aim(Vector2 touchLoc)
    {
        laserBeam.enabled = true;
        aimIndicator.SetActive(true);

        // Check if pointing at object with a teleport collider (valid surface or barrier)
        if (Physics.Raycast(transform.position, transform.forward, out hit, 15, teleportMask))
        {   
            teleportLocation = hit.point;
        }
        else
        {   // If not aimed at a valid surface, target 15 units forward in 3D space
            teleportLocation = transform.forward * range + transform.position;

            // Cast a second ray down to find a surface beneath target
            if (Physics.Raycast(teleportLocation, Vector3.down, out groundRay, range * 1.5f, teleportMask))
                teleportLocation = groundRay.point; 
            else
                teleportLocation = transform.position;
        }

        aimIndicator.transform.position = teleportLocation;
        laserBeam.SetPosition(0, transform.position);
        laserBeam.SetPosition(1, teleportLocation);
    }

    public void Confirm()
    {
        transform.position = teleportLocation;

        if (transform.position.y < 5f)
            SetOnGround();

        // Adjust so teleport destination centered to player's head rather than the center of the playspace
        headOffset = rig.transform.position - playerHead.transform.position;
        teleportLocation += new Vector3(headOffset.x, 0f, headOffset.z);
        rig.transform.position = teleportLocation;

        laserBeam.enabled = false;
        aimIndicator.SetActive(true);
    }

    void SetOnGround()
    {
        if (Physics.Raycast(transform.position + Vector3.up * 200f, Vector3.down, out hit, 200f, teleportMask))
            teleportLocation = hit.point;
    }
}
