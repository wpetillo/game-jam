﻿using UnityEngine;

public class CycleMenu : MonoBehaviour 
{
	[SerializeField] GameObject[] icons;
	public int currentIndex;	

    public void Display()
    {
        icons[currentIndex].SetActive(true);
    }

    public void Hide()
    {
        icons[currentIndex].SetActive(false);
    }

    public void ChangeItem(Vector2 touchLoc)
    {
        if (touchLoc.x < -0.2f)
            MenuLeft();
        else if (touchLoc.x > 0.2f)
            MenuRight();
    }

	private void MenuLeft()
	{
        icons[currentIndex].SetActive(false);
        currentIndex--;

		if (currentIndex < 0)
            currentIndex = icons.Length - 1;

        icons[currentIndex].SetActive(true);
	}

	private void MenuRight()
	{
        icons[currentIndex].SetActive(false);
        currentIndex++;

		if (currentIndex > icons.Length - 1)
            currentIndex = 0;

        icons[currentIndex].SetActive(true);
	}
}


// From variable declarations:
//public GameObject rabbitPool, foxPool, wolfPool, treeSeedPool, fireballPool;
//[HideInInspector] public ObjectSpawner rabbitSpawner, foxSpawner, wolfSpawner;
//[HideInInspector] public ObjectSpawner treeSeedSpawner, fireballSpawner;
//[SerializeField] GameObject grassSeedPrefab, waterBlobPrefab;
//const int GRASS_SEED = 0;
//const int RABBIT = 1;
//const int FOX = 2;
//const int WOLF = 3;
//const int TREE_SEED = 4;
// const int FIREBALL = 5;
//const int WATER = 6;

// From Start:
//currentObject = GRASS_SEED;
/*rabbitSpawner = rabbitPool.GetComponent<ObjectSpawner>();
foxSpawner = foxPool.GetComponent<ObjectSpawner>();
wolfSpawner = wolfPool.GetComponent<ObjectSpawner>();
treeSeedSpawner = treeSeedPool.GetComponent<ObjectSpawner>();
fireballSpawner = fireballPool.GetComponent<ObjectSpawner>();*/

// From SpawnCurrentObject:
/*switch (currentObject)
{
    case GRASS_SEED: Instantiate(grassSeedPrefab, hand.gameObject.transform); break;
    case RABBIT: rabbitSpawner.SpawnObj(spawnLoc); break;
    case FOX: foxSpawner.SpawnObj(spawnLoc); break;
    case WOLF: wolfSpawner.SpawnObj(spawnLoc); break;
    case TREE_SEED:
        SeedController newTreeSeed = treeSeedSpawner.SpawnObj(spawnLoc).GetComponent<SeedController>();
        newTreeSeed.instantPlant = true; break;
    case FIREBALL: fireballSpawner.SpawnObj(spawnLoc); break;
    case WATER: Instantiate(waterBlobPrefab, hand.gameObject.transform); break;
}*/
//Debug.Log("RequestInteraction called from SpawnCurrentObject method");


