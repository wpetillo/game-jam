﻿using UnityEngine;

[System.Serializable]
public class RadialItem
{
    public GameObject baseImage;
    private Renderer renderer;

    [HideInInspector] public float startBound;
    [HideInInspector] public float endBound;


    public void SetBounds(float startBound, float endBound)
    {
        this.startBound = startBound;
        this.endBound = endBound;
    }

    public bool IsInBounds(Vector2 touchPosition)
    {
        return false;
    }

    public void SetBackgroundColor(Color color)
    {
        if (renderer == null)
            renderer = baseImage.GetComponent<Renderer>();

        renderer.material.color = color;
    }
}
