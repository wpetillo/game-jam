﻿using UnityEngine;

public class RadialMenu : MonoBehaviour 
{
    VRKit_Math math = new VRKit_Math();

    [SerializeField] float radialDiameter;
    [SerializeField] Color standard, highlight;
    [SerializeField] RadialItem[] menuItems;

    float touchAngle;
    int currentSelection;

    public IntEvent OnSelect;

    private void Start()
	{
        SetItemPositions();
        SetItemBounds();
        Hide();
    }

    void SetItemPositions()
    {
        float direction;
        Transform item;

        for (int i = 0; i < menuItems.Length; i++)
        {
            direction = i * (360 / menuItems.Length);

            item = menuItems[i].baseImage.transform;
            item.localEulerAngles = new Vector3(0f, direction, 0f);            
            item.Translate(transform.forward * radialDiameter);
            item.localEulerAngles = new Vector3(90f, 0f, 0f);
        }
    }

    private void SetItemBounds()
    {
        float slice = 360 / menuItems.Length;
        float startBound = 360 - (slice / 2);
        float endBound = (slice / 2);

        menuItems[0].SetBounds(startBound, endBound);

        for (int i = 1; i < menuItems.Length; i++)
        {
            startBound = -(slice/2) + (i * slice);
            endBound = (slice/2) + (i * slice);

            menuItems[i].SetBounds(startBound, endBound); 
        }
    }

    public void Display()
    {
        foreach (RadialItem item in menuItems)
        {
            item.baseImage.SetActive(true);
            item.SetBackgroundColor(standard);
        }
    }

	public void Highlight(Vector2 touchLocation)
	{
        this.touchAngle = math.Vector2Degrees(touchLocation);
        currentSelection = GetSelection();

        foreach (RadialItem item in menuItems)
            item.SetBackgroundColor(standard);

        menuItems[currentSelection].SetBackgroundColor(highlight);
	}

    private int GetSelection()
    {
        if (touchAngle > menuItems[0].startBound || touchAngle < menuItems[0].endBound)
            return 0;

        for (int i = 1; i < menuItems.Length; i++)
            if (touchAngle > menuItems[i].startBound && touchAngle < menuItems[i].endBound)
                return i;

        Debug.LogError("GetSelection failed");
        return 0;
    }

    public void Select()
    {
        OnSelect.Invoke(currentSelection);
    }

    public void Hide()
    {
        foreach (RadialItem item in menuItems)
        {
            item.baseImage.SetActive(false);
            item.SetBackgroundColor(standard);
        }
    }

    public void SetItemImage(int index, Material material)
    {
        menuItems[index].baseImage.GetComponent<Renderer>().material = material;
    }
}
