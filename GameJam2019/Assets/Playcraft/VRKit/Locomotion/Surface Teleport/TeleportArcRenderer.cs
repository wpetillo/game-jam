﻿using UnityEngine;

public class TeleportArcRenderer : MonoBehaviour
{
    public float baseRange = 7.5f;
    [SerializeField] int resolution = 40;
    [SerializeField] Transform source;
    [SerializeField] GameObject aimIndicator;
    [SerializeField] LayerMask teleportMask;

    [HideInInspector] public float strength;
    public Vector3 destination { get; private set; }

    float range = 7.5f;
    float angle = 0;
    float height = 5;
	
    float gravity;
    float radianAngle;
    LineRenderer arc;

    private void Awake()
    {
        arc = GetComponent<LineRenderer>();
        gravity = Mathf.Abs(Physics2D.gravity.y);
    }

    private void OnEnable()
    {
        arc.enabled = true;
        aimIndicator.SetActive(true);
    }

    private void OnDisable()
    {
        arc.enabled = false;
        aimIndicator.SetActive(false);
    }

    public void Update()
    {
        if (arc == null)
            return;

		range = baseRange * strength;
        height = baseRange / 2f;
        angle = -source.localEulerAngles.x;
        transform.position = source.position;
        transform.eulerAngles = new Vector3(0f, source.eulerAngles.y, 0f);

        RenderArc();
    }

    public void SetDimensions(float range, float width, Vector3 aimScale)
    {
        baseRange = range;
        arc.startWidth = width;
        arc.endWidth = width;
        aimIndicator.transform.localScale = aimScale;
    }

    // Populate the line renderer with the appropriate settings
    private void RenderArc()
    {
        arc.positionCount = resolution + 1;
        arc.SetPositions(CalculateArcArray());
    }

    // Create array of arc positions
    private Vector3[] CalculateArcArray()
    {
        Vector3[] arcArray = new Vector3[resolution + 1];

        radianAngle = Mathf.Deg2Rad * angle;
        float maxDistance = ((range * Mathf.Cos(radianAngle)) / gravity) * 
            ((range * Mathf.Sin(radianAngle) + Mathf.Sqrt(Mathf.Pow(range * Mathf.Sin(radianAngle), 2) + (2 * gravity * height))));

        for (int i = 0; i <= resolution; i++)
        {
            float t = (float)i / (float)resolution; // Proportion distance of point along arc
            arcArray[i] = CalculateArcPoint(t, maxDistance);

            if (i > 0)
                if (CheckForEndpoint(arcArray[i - 1], arcArray[i]))
                    return CutoffArc(i, arcArray);
        }

        aimIndicator.SetActive(false);
        destination = Vector3.zero;
        return arcArray;
    }

    // Calculate height and distance of each arc vertex
    private Vector3 CalculateArcPoint(float t, float maxDistance)
    {
        float z = t * maxDistance;
        float y = z * Mathf.Tan(radianAngle) - ((gravity * z * z) / (2 * range * range * Mathf.Cos(radianAngle) * Mathf.Cos(radianAngle)));
        return new Vector3(0f, y, z);
    }

    private bool CheckForEndpoint(Vector3 last, Vector3 current)
    {
        last = transform.TransformPoint(last);
        current = transform.TransformPoint(current);

        RaycastHit hit;
        var heading = current - last;
        var distance = heading.magnitude;
        var direction = heading / distance;

        if (Physics.Raycast(last, direction, out hit, distance, teleportMask))
        {
            destination = hit.point;

            aimIndicator.SetActive(true);
            aimIndicator.transform.position = destination;

            return true;
        }
        else return false;
    }

    private Vector3[] CutoffArc(int t, Vector3[] fullArc)
    {
        Vector3[] arcArray = new Vector3[t + 1];

        for (int i = 0; i < t + 1; i++)
            arcArray[i] = fullArc[i];

        arc.positionCount = t + 1;
        return arcArray;
    }
}
