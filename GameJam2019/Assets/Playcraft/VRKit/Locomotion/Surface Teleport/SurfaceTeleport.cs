﻿using UnityEngine;

public class SurfaceTeleport : MonoBehaviour, ILocomotion
{
    [SerializeField] GameObject rig, eye;

    TeleportArcRenderer arcRenderer;
    Vector3 headOffset;

    public void Begin()
    {
        arcRenderer.enabled = true;
    }

    public void Aim(Vector2 input)
    {
        if (arcRenderer == null)
            return;

        arcRenderer.strength = input.y + 1.25f;
    }

    public void Stop()
    {
        if (arcRenderer.destination != Vector3.zero)
            Teleport(arcRenderer.destination);

        arcRenderer.enabled = false;
    }

    private void Awake()
    {
        arcRenderer = GetComponent<TeleportArcRenderer>();
        arcRenderer.enabled = false;
    }

    private void Teleport(Vector3 destination)
    {
        headOffset = rig.transform.position - eye.transform.position;
        destination += new Vector3(headOffset.x, 0f, headOffset.z);
        rig.transform.position = destination;
    }
}
