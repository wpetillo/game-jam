﻿using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public enum LocomotionStrategy { SurfaceTeleport, AvatarControl, SpaceTeleport }

public interface ILocomotion
{
    void Begin();
    void Aim(Vector2 input);
    void Stop();
}

public class LocomotionController : MonoBehaviour
{
    [SerializeField] SteamVR_Input_Sources thisHand;
    [SerializeField] SteamVR_Action_Vector2 Aim;
    [SerializeField] SteamVR_Action_Boolean ClickUpDown;
    [SerializeField] SteamVR_Action_Boolean TouchUpDown;

    [SerializeField] LocomotionStrategy startingStrategy;
    LocomotionStrategy locomotionStrategy;
    [SerializeField] GameObject surfaceTeleportContainer, spaceTeleportContainer;//, avatarControlContainer;
    ILocomotion locomotion;
    ILocomotion surfaceTeleport, spaceTeleport;//, avatarControl;
    bool hasStarted;

    private void Awake()
    {
        surfaceTeleport = surfaceTeleportContainer.GetComponent<ILocomotion>();
        spaceTeleport = spaceTeleportContainer.GetComponent<ILocomotion>();
        //avatarControl = avatarControlContainer.GetComponent<ILocomotion>();
    }

    private void OnEnable()
    {
        SetStrategy(startingStrategy);

        if (ClickUpDown != null)
            ClickUpDown.AddOnChangeListener(OnClickChanged, thisHand);
        if (TouchUpDown != null)
            TouchUpDown.AddOnChangeListener(OnTouchChanged, thisHand);
    }

    private void OnDisable()
    {
        if (ClickUpDown != null)
            ClickUpDown.RemoveOnChangeListener(OnClickChanged, thisHand);
        if (TouchUpDown != null)
            TouchUpDown.RemoveOnChangeListener(OnTouchChanged, thisHand);
    }

    // Begin teleport on press down of touchpad
    private void OnClickChanged(SteamVR_Action_In actionIn)
    {
        if (ClickUpDown.GetStateDown(thisHand))
        {
            hasStarted = true;
            locomotion.Begin();
        }
    }

    // End teleport on touch up of touchpad
    private void OnTouchChanged(SteamVR_Action_In actionIn)
    {
        if (TouchUpDown.GetStateUp(thisHand) && hasStarted)
        {
            hasStarted = false;
            locomotion.Stop();
        }
    }

    private void Update()
    {
        if (hasStarted)
            OnStep(Aim.GetAxis(thisHand));
    }

    public void SetStrategy(LocomotionStrategy locomotionStrategy)
    {
        this.locomotionStrategy = locomotionStrategy;

        surfaceTeleportContainer.SetActive(false);
        spaceTeleportContainer.SetActive(false);
        //avatarControlContainer.SetActive(false);

        switch (locomotionStrategy)
        {
            case LocomotionStrategy.SurfaceTeleport:
                surfaceTeleportContainer.SetActive(true);
                locomotion = surfaceTeleport;
                break;
            case LocomotionStrategy.SpaceTeleport:
                spaceTeleportContainer.SetActive(true);
                locomotion = spaceTeleport;
                break;
            //case LocomotionStrategy.AvatarControl: locomotion = avatarControl; break;
        }
    }

    public void OnStart()
    {

    }

    public void OnStep(Vector2 input)
    {
        if (locomotion == null)
            return;
        if (!hasStarted)
            return;

        locomotion.Aim(input);
    }
}
