﻿using UnityEngine;
 
public class SpaceTeleport : MonoBehaviour, ILocomotion
{
    [SerializeField] GameObject rig, playerHead, teleDestOrb;
    [SerializeField] Transform source;
    [SerializeField] float distanceMultiplier;
    [SerializeField] LayerMask teleportBlocker;

    LineRenderer laserBeam;
    RaycastHit hit;
    Vector3 startOrbScale;
    Vector3 teleDest, telePoint, headOffset;
    float distAim;
    float startLaserWidth;

    private void Awake()
    {
        laserBeam = GetComponent<LineRenderer>();
    }

    private void Start()
    {
        laserBeam.enabled = false;

        startLaserWidth = laserBeam.startWidth;
        startOrbScale = teleDestOrb.transform.localScale;
    }

    public void Begin()
    {
        laserBeam.enabled = true;
        teleDestOrb.SetActive(true);
    }

    public void Aim(Vector2 touchLoc)
    {
        if (touchLoc.y < -0.75f)    //disable teleport if touching bottom of pad
            distAim = 0;
        else
            distAim = (touchLoc.y + 0.75f) * distanceMultiplier;

        if (Physics.Raycast(source.position, source.forward, out hit, distAim * rig.transform.localScale.x, teleportBlocker))
        {
            float dist = Vector3.Distance(source.position, hit.point);
            Vector3 shortTarget = source.position + (dist * Vector3.Normalize(hit.point - source.position));
            teleDestOrb.transform.position = shortTarget;   //if aiming teleport beam at a boundary, stop beam at the boundary

            teleDest = shortTarget;
        }
        else
        {
            teleDest = new Vector3(source.forward.x * distAim * rig.transform.localScale.x + source.position.x,
                                source.forward.y * distAim * rig.transform.localScale.y + source.position.y,
                                source.forward.z * distAim * rig.transform.localScale.z + source.position.z);

            teleDestOrb.transform.position = teleDest;
        }

        laserBeam.SetPosition(0, source.position);
        laserBeam.SetPosition(1, teleDest);
    }

    public void Stop()
    {
        teleDestOrb.SetActive(false);
        laserBeam.enabled = false;

        if (distAim == 0f)
            return;

        // Adjust so teleport destination centered to player's head rather than the center of the playspace
        headOffset = rig.transform.position - playerHead.transform.position;
        teleDest += headOffset;     

        rig.transform.position = teleDest;
    }

    public void AdjustLaserToRigScale(float rigScale)
    {
        teleDestOrb.transform.localScale = rigScale * startOrbScale;

        laserBeam.startWidth = rigScale * startLaserWidth;
        laserBeam.endWidth = rigScale * startLaserWidth;
    }
}
