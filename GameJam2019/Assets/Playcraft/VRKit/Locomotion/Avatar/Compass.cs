﻿using UnityEngine;

public class Compass : MonoBehaviour
{
    [SerializeField] Transform needle;
    [SerializeField] LayerMask groundLayer;

    private void Update()
    {
        transform.rotation = Quaternion.Euler(new Vector3(0f, transform.eulerAngles.y, 0f));
    }

    public Vector3 GetHeading(Vector3 actorLocation, Vector2 input)
    {
        needle.localPosition = SetOnGround(actorLocation, new Vector3(input.x, 0f, input.y));
        return needle.position - transform.position;
    }

    RaycastHit hit;
    private Vector3 SetOnGround(Vector3 actorLocation, Vector3 input)
    {
        if (Physics.Raycast(actorLocation + input + Vector3.up * 20f, Vector3.down * 30f, out hit, groundLayer))
            return hit.point - actorLocation;
        else
            return input;
    }
}