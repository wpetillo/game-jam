﻿using UnityEngine;

// Converts player touchpad input into animal targeting commands
public class AvatarMove : MonoBehaviour, ILocomotion
{
    [SerializeField] Transform rig, playerEye;
    [SerializeField] Transform carrot;
    [HideInInspector] public Character avatar;
    [SerializeField] Compass compass;
    Vector3 headOffset;

    Vector3 direction;
    Quaternion goalDirection;
    float vertOffset;
    bool isSprinting;

    public void SetAvatar(Character avatar)
    {
        this.avatar = avatar;
    }

    public void Begin()
    {
        //if (avatar.actionState.currentAction == ActionType.RunTowards)
        //    isSprinting = true;

        //avatar.actionState.SetState(ActionType.WalkTowards);
        //avatar.moveState.SetTarget(carrot);
    }

    // Tell the avatar which direction to move
    public void Aim(Vector2 input)
    {
        direction = compass.GetHeading(avatar.transform.position, input);
        carrot.position = avatar.transform.position + direction;

        SetAction(direction.magnitude);
    }

    private void SetAction(float magnitude)
    {
        //if (magnitude > 0.75 && isSprinting)
        //    avatar.actionState.SetState(ActionType.Sprint);
        //else if (magnitude > 0.666f)
        //    avatar.actionState.SetState(ActionType.RunTowards);
        //else if (magnitude < 0.5f)
        //    avatar.actionState.SetState(ActionType.WalkTowards);
    }

    public bool RequestStop(bool isTouchUp)
    {
        isSprinting = false;
        return isTouchUp;
    }

    // Teleport to avatar
    public void Stop()
    {
        headOffset = rig.transform.position - playerEye.transform.position;

        //avatar.actionState.SetState(ActionType.SnapToTarget);
        rig.transform.position = avatar.transform.position + new Vector3(headOffset.x, 0f, headOffset.z);    
    }
}
