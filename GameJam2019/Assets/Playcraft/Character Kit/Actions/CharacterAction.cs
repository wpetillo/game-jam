﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "Action", menuName = "Playcraft/Character Kit/Actions/Generic")]
public class CharacterAction : ScriptableObject, IAction
{
    [SerializeField] AnimationType animation;
    [SerializeField] MoveType movement;

    enum MoveSpeed { Still, Walk, Run, Sprint }
    [SerializeField] MoveSpeed moveSpeed;
    enum TurnSpeed { Still, Slow, Medium, Fast }
    [SerializeField] TurnSpeed turnSpeed;
    enum EntrySound { None, Eat, Attack }
    [SerializeField] EntrySound entrySound;
    //Sound currentSound;

    public float timeToComplete;
    [SerializeField] int energyCost;

    public virtual bool RequestEntry(Character character)
    {
        EnterState(character);
        return true;
    }

    protected virtual void EnterState(Character character)
    {
        character.SetAnimation(animation);
        character.SetMoveState(movement, GetMoveSpeed(character, moveSpeed), GetTurnSpeed(character, turnSpeed));
        PlaySound(character);
        //Debug.Log(GetMoveSpeed(character, moveSpeed) + " " + GetTurnSpeed(character, turnSpeed));
    }

    private float GetMoveSpeed(Character character, MoveSpeed speed)
    {
        switch (speed)
        {
            case MoveSpeed.Still: return 0f;
            case MoveSpeed.Walk: return character.attributes.walkSpeed;
            case MoveSpeed.Run: return character.attributes.runSpeed;
            case MoveSpeed.Sprint: return character.attributes.sprintSpeed;
            default: Debug.LogError("Invalid move speed"); return 0f;
        }
    }

    private float GetTurnSpeed(Character character, TurnSpeed speed)
    {
        switch (speed)
        {
            case TurnSpeed.Still: return 0f;
            case TurnSpeed.Slow: return character.attributes.slowTurnSpeed;
            case TurnSpeed.Medium: return character.attributes.mediumTurnSpeed;
            case TurnSpeed.Fast: return character.attributes.fastTurnSpeed;
            default: Debug.LogError("Invalid turn speed"); return 0f;
        }
    }

    private void PlaySound(Character character)
    {
        /*switch (entrySound)
        {
            case EntrySound.None: return;
            case EntrySound.Eat: currentSound = character.data.attributes.eatSound; break;
            case EntrySound.Attack: currentSound = character.data.attributes.attackSound; break;
            default: Debug.LogError("Invalid sound"); return;
        }

        if (currentSound.isSilent || currentSound == null)
            return;

        SoundManager.instance.PlayOnce(currentSound, character.transform.position);*/
    }

    public virtual void EndCooldown(Character character) { }

    public float GetTimeToComplete()
    {
        return timeToComplete;
    }

    public int GetEnergyCost()
    {
        return energyCost * Mathf.RoundToInt(timeToComplete);
    }
}
