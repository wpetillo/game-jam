﻿public interface IAction
{
    bool RequestEntry(Character character);
    int GetEnergyCost();
    float GetTimeToComplete();
    void EndCooldown(Character character);
}