﻿using UnityEngine;

public class ActionController
{
    Character character;
    public IAction currentAction;
    bool canEnter;

    public ActionController(Character character, IAction startingAction)
    {
        this.character = character;
        currentAction = startingAction;
    }

    public int GetCaloriesBurned()
    {
        return character.attributes.energyBurnRate * currentAction.GetEnergyCost();
    }

    public float GetTimeToComplete()
    {
        return currentAction.GetTimeToComplete();
    }

    public bool SetState(IAction action)
    {
        if (action == null)
        {
            Debug.LogError("Attempting to set character state to a null action");
            return false;
        }

        //Debug.Log("Setting action state of " + character.gameObject.name + " to " + action.ToString());
        currentAction = action;
        canEnter = currentAction.RequestEntry(character);
        return canEnter;
    }

    public void EndCooldown(Character character, IAction action)
    {
        action.EndCooldown(character);
    }
}
