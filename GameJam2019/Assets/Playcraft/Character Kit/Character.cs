﻿using System;
using UnityEngine;

public class Character : MonoBehaviour
{
    public Attributes attributes;

    private CharacterData data;
    public CharacterData Data
    {
        get
        {
            if (data == null)
                data = new CharacterData(attributes);

            return data;
        }
    }

    private BrainController brain;
    public BrainController Brain
    {
        get
        {
            if (brain == null)
                brain = new BrainController(this);

            return brain;
        }
    }

    private Perception perception;
    public Perception Perception
    {
        get
        {
            if (perception == null)
                perception = new Perception(this, attributes);

            return perception;
        }
    }

    private MovementController movement;
    public MovementController Movement
    {
        get
        {
            if (movement == null)
                movement = new MovementController(this);

            return movement;
        }
    }

    private ActionController action;
    public ActionController Action
    {
        get
        {
            if (action == null)
                action = new ActionController(this, attributes.Abilities.GetDefault());

            return action;
        }
    }

    [SerializeField] Transform animationContainer;
    AnimationController animationState;
    Animator animator;

    Action _decide;

    private void Awake()
    {
        _decide = SetNextAction;
    }

    private void OnEnable()
    {
        if (attributes.characterType == CharacterType.NPC)
            SetNextAction();
    }

    private void SetNextAction()
    {
        if (Action.GetTimeToComplete() == 0f)
        {
            Debug.LogError("Trying to invoke method with zero time delay, cancelling invoke");
            return;
        }

        // Call this method again
        MonoBehaviourExtensions.Invoke(this, _decide, action.GetTimeToComplete());

        Brain.React();
    }

    public void SetAnimation(AnimationType anim)
    {
        if (animationState == null)
            animationState = new AnimationController(GetAnimator());

        animationState.SetAnimation(anim);
    }

    private Animator GetAnimator()
    {
        foreach (Transform child in animationContainer)
        {
            if (child.gameObject.activeSelf)
            {
                Animator anim = child.gameObject.GetComponent<Animator>();

                if (anim != null)
                    return anim;
            }
        }

        Debug.LogError("No animator found! Make sure animationContainer has at least one active child with an Animator component attached");
        return null;
    }

    public void SetMoveState(MoveType state, float moveSpeed, float turnSpeed)
    {
        Movement.SetState(state);
        Movement.SetSpeed(moveSpeed, turnSpeed);
    }

    public void ReceiveCommand(MoveCommand command)
    {
        Movement.ApplyDirectControl(command);
    }

    private void Update()
    {
        Movement.Step();

        if (brain != null)
            brain.Step();
    }
}
