﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Attributes", menuName = "Playcraft/Character Kit/Attributes/Human NPC")]
public class NPCAttributes : Attributes
{
    [SerializeField] NPCAbilities abilities;
    protected override Abilities GetAbilities()
    { return abilities; }

    [SerializeField] Attributes[] threats;
    protected override Attributes[] GetThreats()
    { return threats; }

    [SerializeField] Attributes[] targets;
    protected override Attributes[] GetTargets()
    { return targets; }

    [SerializeField] Senses senses;
    protected override Senses GetSenses()
    { return senses; }
}

[Serializable]
public class NPCAbilities : Abilities
{
    public CharacterAction idle;
    public CharacterAction wander;
    public CharacterAction dead;
    public CharacterAction runAway;
    public CharacterAction scatter;
    public CharacterAction runTowards;
    public CharacterAction attack;

    public override CharacterAction GetDefault()
    {
        return idle;
    }
}

[Serializable]
public class Senses
{
    public float visionDistance;
    public float visionWidth;
    public float sniffRange;
}
