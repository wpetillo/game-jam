﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Attributes", menuName = "Playcraft/Character Kit/Attributes/Human Player")]
public class PlayerAttributes : Attributes
{
    [SerializeField] PlayerAbilities abilities;
    protected override Abilities GetAbilities()
    { return abilities; }
}

[Serializable]
public class PlayerAbilities : Abilities
{
    public CharacterAction idle;

    public override CharacterAction GetDefault()
    {
        return idle;
    }
}
