﻿using System.Collections.Generic;
using UnityEngine;

// Contains all data belonging to an individual animal
public class CharacterData 
{
    // Unchanging data
    private Attributes attributes;

    // Evolving data
    public Traits traits;

    // Data containers
    public Focus focus;
    public Observations observations;

    // Changing stats
    public int calorieCount;
    public int fatCount;
    public int calorieValue;
    public int health;
    public int offspringCount;
    public int searchFailCount;
    public int panicLevel;

    // Action cooldowns
    public bool eatCoolingDown;
    public bool attackCoolingDown;


    public CharacterData(Attributes attributes)
    {
        this.attributes = attributes;

        focus = new Focus();
        observations = new Observations();

        Reset();
    }

    public void Reset()
    {
        health = attributes.maxHealth;

        fatCount = 0;
        offspringCount = 0;
        searchFailCount = 0;
    }

    public void Copy(CharacterData source)
    {
        calorieCount = source.calorieCount;
        fatCount = source.fatCount;
        health = source.health;
        calorieValue = source.calorieValue;
    }
}

public struct Traits
{
    public const float MIN_EXPLORING = 0.1f;
    public const float MAX_EXPLORING = 5f;
    public float exploring;

    public const float MIN_EVASIVENESS = 1f;
    public const float MAX_EVASIVENESS = 5f;
    public float evasiveness;

    public const float MIN_EAGERNESS = 0.5f;
    public const float MAX_EAGERNESS = 5f;
    public float eagerness;
}

public struct Focus
{
    public Collider Body;
}

public class Observations
{
    public List<Collider> targetsInSight = new List<Collider>();
    public List<Collider> threatsInSight = new List<Collider>();

    public void Clear()
    {
        targetsInSight.Clear();
        threatsInSight.Clear();
    }
}

