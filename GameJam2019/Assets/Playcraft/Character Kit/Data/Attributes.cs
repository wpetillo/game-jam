﻿using System;
using UnityEngine;

// Stores persistent (initialization) data for a character
public class Attributes : ScriptableObject
{
    public CharacterType characterType;
    //public Sound eatSound, attackSound;

    public float reactionTime;
    public float attackRange;
    public float attackCooldownTime;
    public float burnTime;

    public int fightPower;          //Ability to defend self against threats or rivals
    public int maxHealth;
    public int maxPanic;

    public int energyBurnRate;
    public float slowTurnSpeed, mediumTurnSpeed, fastTurnSpeed;
    public float walkSpeed, runSpeed, sprintSpeed;

    public float jumpStrength;
    public bool canDoubleJump;

    public Vector3 fireScale;
    public Vector3 modelScale;
    public float modelHeightOffset;
    public Vector3 bodyOffsetOnDeath;

    public Abilities Abilities
    { get { return GetAbilities(); } }
    protected virtual Abilities GetAbilities() { return null; }

    public Attributes[] Threats
    { get { return GetThreats(); } }
    protected virtual Attributes[] GetThreats() { return null; }

    public Attributes[] Targets
    { get { return GetTargets(); } }
    protected virtual Attributes[] GetTargets() { return null; }

    public Senses Senses
    { get { return GetSenses(); } }
    protected virtual Senses GetSenses() { return null; }
}

public abstract class Abilities
{
    public virtual CharacterAction GetDefault() { return null; }
}
