﻿using UnityEngine;

public class Detectable : MonoBehaviour
{
    public Character character;

    public Vector3 GetLocation()
    {
        return character.transform.position;
    }
}
