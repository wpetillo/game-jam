﻿using System.Collections.Generic;
using UnityEngine;

public class Perception
{
    Character character;
    Attributes attributes;

    RaycastHit[] visibleObjs;
    RaycastHit hitSight;
    Collider[] smelledObjs;
    string currentOtherTag;
    Collider currentTarget;
    float currentDistanceToTarget;

    public Perception(Character character, Attributes attributes)
    {
        this.character = character;
        this.attributes = attributes;
    }

    public void Look()
    {
        //Debug.Log("Look method called in Perception class");
        character.Data.observations.Clear();

        visibleObjs = Physics.SphereCastAll(character.transform.position + (character.transform.forward * attributes.Senses.visionWidth / 2f),
            attributes.Senses.visionWidth, character.transform.forward, attributes.Senses.visionDistance);

        foreach (RaycastHit hitObj in visibleObjs)
            Identify(hitObj.collider, true);
    }

    public void Sniff()
    {
        //Debug.Log(character.name + " sniffing...");
        character.Data.observations.Clear();

        smelledObjs = Physics.OverlapSphere(character.transform.position, attributes.Senses.sniffRange);

        foreach (Collider scent in smelledObjs)
            Identify(scent, false);
    }

    // First pass identification checks tags for efficiency
    private void Identify(Collider currCollider, bool canPerceiveThreat)
    {
        if (currCollider == null)
            return;

        currentOtherTag = currCollider.gameObject.tag;

        if (currentOtherTag == "Hazard")
        {
            character.Data.observations.threatsInSight.Add(currCollider);
            return;
        }
        else if (currentOtherTag == "Character")
        {
            //Debug.Log(CanSee(currCollider.transform.position));
            if (!CanSee(currCollider.transform.position))
                return;

            if (IsThreat(currCollider))
            {
                character.Data.observations.threatsInSight.Add(currCollider);
                return;
            }
            if (IsTarget(currCollider))
            {
                //Debug.Log("target acquired");
                character.Data.observations.targetsInSight.Add(currCollider);
                return;
            }
        }
    }

    private bool CanSee(Vector3 targetPosition)
    {
        Vector3 origin = character.transform.position;
        Vector3 direction = targetPosition - origin;
        float distance = Vector3.Distance(origin, targetPosition);

        if (Physics.Raycast(origin, direction, out hitSight, distance))
        {
            //Debug.Log(hitSight.transform.gameObject.layer);
            if (hitSight.transform.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
                return false;
        }

        return true;
    }

    // * Merge with IsTarget
    private bool IsThreat(Collider other)
    {
        var otherData = other.gameObject.GetComponent<Detectable>().character.attributes;

        if (attributes.Threats.Length > 0)
            foreach (var threat in attributes.Threats)
                if (otherData == threat)
                    return true;

        return false;
    }

    private bool IsTarget(Collider other)
    {
        var otherData = other.gameObject.GetComponent<Detectable>().character.attributes;

        if (attributes.Targets.Length > 0)
            foreach (var target in attributes.Targets)
                if (otherData == target)
                    return true;

        return false;
    }

    // Determines if character is close enough to target that it no longer needs to pursue
    public bool IsInRangeOfTarget(float minRange)
    {
        currentTarget = character.Data.focus.Body;

        if (currentTarget == null)
            return false;

        currentDistanceToTarget = Vector3.Distance(currentTarget.transform.position, character.transform.position);

        return (currentDistanceToTarget <= minRange);
    }

    // Sort through multiple objects of interest to find the one that is closest
    public Collider FindClosest(Vector3 position, List<Collider> visibleCols)
    {
        Collider closest = null;
        float dist = Mathf.Infinity;

        foreach (Collider c in visibleCols)
        {
            //Debug.Log(c.gameObject.name + " found.");
            float d = Vector3.Distance(position, c.transform.TransformPoint(Vector3.zero));

            if (closest == null || d < dist)
            {
                closest = c;
                dist = d;
            }
        }

        return closest;
    }
}