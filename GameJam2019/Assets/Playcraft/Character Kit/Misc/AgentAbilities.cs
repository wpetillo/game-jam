﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Agent Abilities", menuName = "Playcraft/Character Kit/General Abilities")]
public class AgentAbilities : ScriptableObject
{
    public Collider FindClosest(Vector3 position, List<Collider> visibleCols)   
    {
        Collider closest = null;
        float dist = Mathf.Infinity;

        foreach (Collider c in visibleCols)
        {
            //Debug.Log(c.gameObject.name + " found.");
            float d = Vector3.Distance(position, c.transform.TransformPoint(Vector3.zero));

            if (closest == null || d < dist)
            {
                closest = c;
                dist = d;
            }
        }

        return closest;
    }
}
