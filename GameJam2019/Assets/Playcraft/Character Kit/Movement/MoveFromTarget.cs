﻿using UnityEngine;

public class MoveFromTarget : IMoveStrategy
{
    Transform self, target;
    IMoveRotate movement;

    public MoveFromTarget(IMoveRotate movement)
    {
        this.movement = movement;
        self = movement.GetTransform();
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }

    public void Step()
    {
        movement.RotateStep(target, false);
        movement.MoveForwardStep();
    }
}
