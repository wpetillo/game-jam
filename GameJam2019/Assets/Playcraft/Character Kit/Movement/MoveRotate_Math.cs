﻿using UnityEngine;

public class MoveRotate_Math : MonoBehaviour, IMoveRotate
{
    float moveSpeed, turnSpeed;

    float rotationStep;
    Vector3 targetPosition;
    Vector3 targetDirection;
    Vector3 newDirection;

    RaycastHit leftBack, rightBack, front;
    Vector3 leftSide, rightSide, groundNormal;
    float xSlope, zSlope;
    Vector2 yzu, yzv, xyu, xyv;

    [SerializeField] Transform tripodFront, tripodLeft, tripodRight;

    Rigidbody rb;

    public void SetSpeed(float moveSpeed, float turnSpeed)
    {
        //Debug.Log("Setting turn speed of " + gameObject.name + " to " + turnSpeed);
        this.moveSpeed = moveSpeed;
        this.turnSpeed = turnSpeed;
    }

    public Transform GetTransform()
    {
        return transform;
    }

    // * Defect: this form of movement should not rely on a rigidbody being present
    public Rigidbody GetRigidbody()
    {
        if (rb == null)
            rb = GetComponent<Rigidbody>();

        return rb;
    }

    // Turn in a specified direction
    public void RotateStep(int direction)
    {
        if (direction == 0)
            return;

        transform.Rotate(Vector3.up * turnSpeed * direction * Time.deltaTime, Space.World);
    }

    // Turn towards a target
    public void RotateStep(Transform target, bool isTowards)
    {
        //Debug.Log(gameObject.name + " is turning " + isTowards + " target " + target.gameObject.name); 
        rotationStep = turnSpeed * Mathf.Deg2Rad * Time.deltaTime;
        targetPosition = target.position;
        targetPosition.y = transform.position.y;
        targetDirection = (targetPosition - transform.position) * (isTowards? 1 : -1);  
        newDirection = Vector3.RotateTowards(transform.forward, targetDirection, rotationStep, 0f);
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    public void MoveForwardStep()
    {
        SetOnGround();
        AlignToGround();
        transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime, Space.Self);
        //Debug.DrawRay(transform.position, transform.forward, Color.red);
    }

    public void MoveStep(Vector3 direction)
    {
        SetOnGround();
        AlignToGround();
        transform.Translate(direction * moveSpeed * Time.deltaTime, Space.Self);
    }

    // Rotate transform so that it is aligned to the surface it is standing on, if any.
    // Only provides minor aesthetic benefit now that movement turns off physics
    float xSlopeMultiplier = -1f; // * Insufficient on uphill slopes, too much on downhill
    float zSlopeMultiplier = 0f;
    private void AlignToGround()
    {
        Physics.Raycast(tripodLeft.position + Vector3.up, Vector3.down * 1.5f, out leftBack);
        Physics.Raycast(tripodRight.position + Vector3.up, Vector3.down * 1.5f, out rightBack);
        Physics.Raycast(tripodFront.position + Vector3.up, Vector3.down * 1.5f, out front);

        leftSide = leftBack.point - front.point;
        rightSide = rightBack.point - front.point;
        groundNormal = Vector3.Cross(rightSide, leftSide).normalized;
        //Debug.DrawRay(transform.position, groundNormal, Color.red);

        yzu.x = leftSide.y;
        yzu.y = leftSide.z;
        yzv.x = rightSide.y;
        yzv.y = rightSide.z;

        xyu.x = leftSide.x;
        xyu.y = leftSide.y;
        xyv.x = rightSide.x;
        xyv.y = rightSide.y;

        zSlopeMultiplier = -Vector3.Dot(front.transform.forward, transform.forward);
        xSlope = xSlopeMultiplier * Mathf.Acos(Vector2.Dot(yzu, yzv));
        zSlope = zSlopeMultiplier * Mathf.Acos(Vector2.Dot(xyu, xyv));

        if (float.IsNaN(xSlope) || float.IsNaN(zSlope))
            return;

        transform.eulerAngles = new Vector3(xSlope, transform.eulerAngles.y, zSlope);
    }

    RaycastHit ground;
    [SerializeField] float modelVertOffset;
    //[SerializeField] LayerMask groundLayer;   // * TBD: prevent animals from stacking on top of each other
    private void SetOnGround()
    {
        if (Physics.Raycast(transform.position, Vector3.down, out ground, 1f))
            transform.position = new Vector3(ground.point.x, ground.point.y + modelVertOffset, ground.point.z);
    }

    // TBD
    public bool RequestJump(float jumpStrength, JumpType style)
    {
        return true;
    }
}
