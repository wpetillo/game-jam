﻿using UnityEngine;

public class MoveCommand
{
    public Vector2 moveDirection;
    public int turnDirection;

    CharacterAction[] forwardMotionStyles;
    public CharacterAction forwardMotionStyle;
    int forwardMotionIndex;

    public bool isJumping;

    public MoveCommand(CharacterAction[] forwardMotionStyles)
    {
        this.forwardMotionStyles = forwardMotionStyles;
        SetForwardMotionStyle();
    }

    public void Reset()
    {
        moveDirection = Vector2.zero;
        turnDirection = 0;
    }

    public void SpeedUp()
    {
        forwardMotionIndex++;

        if (forwardMotionIndex >= forwardMotionStyles.Length)
            forwardMotionIndex = forwardMotionStyles.Length - 1;

        SetForwardMotionStyle();
    }

    public void SlowDown()
    {
        forwardMotionIndex--;

        if (forwardMotionIndex < 0)
            forwardMotionIndex = 0;

        SetForwardMotionStyle();
    }

    private void SetForwardMotionStyle()
    {
        forwardMotionStyle = forwardMotionStyles[forwardMotionIndex];
    }
}
