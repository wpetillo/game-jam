﻿using UnityEngine;

public interface IMoveRotate
{
    void SetSpeed(float moveSpeed, float turnSpeed);
    Transform GetTransform();
    Rigidbody GetRigidbody();
    void RotateStep(int direction);
    void RotateStep(Transform target, bool isTowards);
    void MoveForwardStep();
    void MoveStep(Vector3 direction);
    bool RequestJump(float jumpStrength, JumpType style);
}
