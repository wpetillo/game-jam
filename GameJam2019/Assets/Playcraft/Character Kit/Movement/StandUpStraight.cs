﻿using UnityEngine;

public class StandUpStraight  
{
    Transform self;

    public StandUpStraight(Transform transform)
    {
        self = transform;
    }

    // Check my current angle to see if I have fallen over
    // Return modified rotation if fallen, original rotation otherwise
    public Quaternion SetUpright()
    {
        float xRot = self.rotation.eulerAngles.x;  
        float zRot = self.rotation.eulerAngles.z;
        //Debug.Log(xRot + " " + zRot);

        if (CheckAngleOver(xRot, 50))    
            return Quaternion.Euler(0, self.rotation.eulerAngles.y, 0);
        else if (CheckAngleOver(zRot, 5))
            return Quaternion.Euler(self.rotation.eulerAngles.x, self.rotation.eulerAngles.y, 0);
        else
            return self.rotation;
    }

    // Check if angle is greater than maxAngle in either direction
    bool CheckAngleOver(float angle, float maxAngle)
    {
        if (angle < 0)          
            angle *= -1;

        if (angle < maxAngle) 
            return false;
        else if (angle > 360 - maxAngle)
            return false;
        else
            return true;
    }
}
