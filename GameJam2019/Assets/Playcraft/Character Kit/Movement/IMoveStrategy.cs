﻿using UnityEngine;

public interface IMoveStrategy
{
    void Step();
    void SetTarget(Transform target);
}
