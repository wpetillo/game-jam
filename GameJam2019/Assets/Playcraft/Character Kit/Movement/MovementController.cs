﻿using UnityEngine;

public class MovementController
{
    Character character;
    IMoveRotate movement;

    enum StrategyType { TargetBased, DirectControl }
    StrategyType strategyType;

    IMoveStrategy state;
    IMoveStrategy toTargetState, fromTargetState, randomState, stillState;
    IMoveStrategy snapToLocationState; 
    public MoveType currentMoveState;

    public MovementController(Character character)
    {
        this.character = character;
        movement = character.GetComponent<IMoveRotate>();
        SetState(MoveType.Still);
    }

    // * Eliminate the need for this conditional by only calling this method in Character when target-based movement is used
    public void Step()
    {
        //Debug.Log("Step method called in MoveState, state is " + currentMoveState.ToString() + ", strategy is " + strategyType.ToString());
        switch (strategyType)
        {
            case StrategyType.TargetBased:
                state.Step();
                break;
            case StrategyType.DirectControl:
                break;
        }
    }

    // Bypasses move strategy abstraction to access movement implementation directly
    public void ApplyDirectControl(MoveCommand command)
    {
        if (command.isJumping)
        {
            command.isJumping = false;

            bool canJumpNormal = movement.RequestJump(character.attributes.jumpStrength, JumpType.Normal);

            if (canJumpNormal)
                character.SetAnimation(AnimationType.Jump);
            else if (!canJumpNormal && character.attributes.canDoubleJump)
            {
                movement.RequestJump(character.attributes.jumpStrength, JumpType.Double);
                character.SetAnimation(AnimationType.Jump);
            }
        }

        movement.MoveStep(new Vector3(command.moveDirection.x, 0f, command.moveDirection.y));
        movement.RotateStep(command.turnDirection);
    }

    public void SetTarget(Transform target)
    {
        //Debug.Log(character.gameObject.name + " setting target to " + target.gameObject.name);
        state.SetTarget(target);
    }

    public void SetSpeed(float moveSpeed, float turnSpeed)
    {
        movement.SetSpeed(moveSpeed, turnSpeed);
    }

    public void SetState(MoveType moveType)
    {
        currentMoveState = moveType;
        //Debug.Log("Changing move state to " + moveType);
        switch (moveType)
        {
            // Target-based movement types
            case MoveType.ToTarget:
                strategyType = StrategyType.TargetBased;
                if (toTargetState == null)
                    toTargetState = new MoveToTarget(movement);
                state = toTargetState;
                break;
            case MoveType.FromTarget:
                strategyType = StrategyType.TargetBased;
                if (fromTargetState == null)
                    fromTargetState = new MoveFromTarget(movement);
                state = fromTargetState;
                break;
            case MoveType.Random:
                strategyType = StrategyType.TargetBased;
                if (randomState == null)
                    randomState = new MoveToRandom(movement);
                state = randomState;
                break;
            case MoveType.Still:
                strategyType = StrategyType.TargetBased;    // * Consider changing type
                if (stillState == null)
                    stillState = new StandStill();
                state = stillState;
                break;
            case MoveType.TrackObject:
                strategyType = StrategyType.TargetBased;    // * Consider changing type
                if (snapToLocationState == null)
                    snapToLocationState = new SnapToLocation(character.transform);
                state = snapToLocationState;
                break;
            // Direct-control movement types
            case MoveType.Free:
                strategyType = StrategyType.DirectControl;
                break; 
            default: Debug.LogError("Invalid movement state: " + moveType); break;
        }
    }
}
