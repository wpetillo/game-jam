﻿using UnityEngine;

public class MoveRotate_Physics : MonoBehaviour, IMoveRotate
{
    [SerializeField] Transform sight;
    StandUpStraight upright;
    float moveSpeed, turnSpeed;
    float jumpStrength;

    float rotationStep;
    Vector3 moveTarget;
    Quaternion newDirection;

    Rigidbody rb;
    bool isOnGround;
    bool isDoubleJumping;

    public void SetSpeed(float moveSpeed, float turnSpeed)
    {
        //Debug.Log("Setting turn speed of " + gameObject.name + " to " + turnSpeed);
        this.moveSpeed = moveSpeed;
        this.turnSpeed = turnSpeed;
    }

    public Transform GetTransform()
    {
        return transform;
    }

    public Rigidbody GetRigidbody()
    {
        if (rb == null)
            rb = GetComponent<Rigidbody>();

        return rb;
    }

    // Turn in a specified direction
    public void RotateStep(int direction)
    {
        // * Previously used by RandomRotation, verify new code works before deleting
        //moveTarget = new Vector3(transform.position.x + direction, transform.position.y, transform.position.z + 1f);
        //sight.LookAt(moveTarget);

        // * Used for Free Rotation, check if also works for target-based rotation...
        sight.eulerAngles = new Vector3(0f, transform.eulerAngles.y + direction * 90f, 0f);

        RotateToTarget();
    }

    // Turn towards a target
    public void RotateStep(Transform target, bool isTowards)
    {
        moveTarget = new Vector3(target.position.x, transform.position.y, target.position.z);

        if (moveTarget == transform.position)   // When burning, fire (target) is in same position as character (transform)
            return;                             // avoid LookRotation error by returning out
        if (isTowards)
            sight.LookAt(moveTarget);
        else
            sight.rotation = Quaternion.LookRotation(transform.position - moveTarget);

        RotateToTarget(); 
    }

    private void RotateToTarget()
    {
        //Debug.Log("Rotating towards target...");
        if (upright == null)
            upright = new StandUpStraight(transform);

        upright.SetUpright();

        rotationStep = turnSpeed * Time.deltaTime;
        newDirection = Quaternion.RotateTowards(transform.rotation, sight.rotation, rotationStep);
        transform.eulerAngles = new Vector3(0f, newDirection.eulerAngles.y, 0f);
    }

    public void MoveForwardStep()
    {
        //Debug.Log("Moving forward step...");
        transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime, Space.Self);
        //Debug.DrawRay(transform.position, transform.forward, Color.red);
    }

    public void MoveStep(Vector3 direction)
    {
        //Debug.Log("Moving in direction " + direction + " at speed " + moveSpeed);
        transform.Translate(direction * moveSpeed * Time.deltaTime, Space.Self);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            isOnGround = true;
            isDoubleJumping = false;
        }
    }

    public bool RequestJump(float jumpStrength, JumpType style)
    {
        bool canJump;

        if (style == JumpType.Normal && isOnGround)
            canJump = true;
        else if (style == JumpType.Double && !isDoubleJumping)
        {
            canJump = true;
            isDoubleJumping = true;
        }
        else
            canJump = false;

        if (canJump)
        {
            this.jumpStrength = jumpStrength;
            isOnGround = false;

            if(isDoubleJumping)
                Jump();
            else
                Invoke("Jump", 0.4f);
        }

        return canJump;
    }

    private void Jump()
    {
        GetRigidbody().velocity = Vector3.up * jumpStrength;
    }
}

