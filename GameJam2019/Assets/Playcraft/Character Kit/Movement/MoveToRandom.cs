﻿using UnityEngine;

public class MoveToRandom : IMoveStrategy
{
    enum Direction { Forward, Left, Right }
    Direction direction;

    IMoveRotate movement;

    public MoveToRandom(IMoveRotate movement)
    {
        this.movement = movement;
    }

    public void SetTarget(Transform x)
    {
        int randomDirection = Random.Range(0, 3);

        switch (randomDirection)
        {
            case 0: direction = Direction.Forward; break;
            case 1: direction = Direction.Left; break;
            case 2: direction = Direction.Right; break;
        }
        //Debug.Log("Random direction set to " + direction);
    }

    public void Step()
    {
        //Debug.Log("Step method called in MoveToRandom " + direction);
        switch (direction)
        {
            case Direction.Forward: movement.RotateStep(0); break;
            case Direction.Left: movement.RotateStep(-1); break;
            case Direction.Right: movement.RotateStep(1); break;
        }

        movement.MoveForwardStep();
    }
}