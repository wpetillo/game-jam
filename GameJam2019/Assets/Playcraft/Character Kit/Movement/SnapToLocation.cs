﻿using UnityEngine;

public class SnapToLocation : IMoveStrategy
{
    Transform self, target;
    int layerMask = 1 << 8;
    Vector3 verticalOffset;

    public SnapToLocation(Transform transform)
    {
        self = transform;
        //verticalOffset = new Vector3(0f, self.GetComponent<Character>().data.attributes.modelHeightOffset, 0f);
    }

    public void SetTarget(Transform target)
    {
        //Debug.Log(self.gameObject.name + " has set target to " + target.gameObject.name);
        this.target = target;
    }

    public void Step()
    {
        if (target == null)
            return;

        //Debug.Log(target.position);
        self.position = SetOnGround(target.position);
        self.rotation = Quaternion.Euler(0f, target.eulerAngles.y, 0f);
    }

    RaycastHit groundHit;
    private Vector3 SetOnGround(Vector3 position)
    {
        if (Physics.Raycast(position + Vector3.up, Vector3.down, out groundHit, 5f, layerMask))
            return groundHit.point + verticalOffset;
        else
            return position;
    }
}
