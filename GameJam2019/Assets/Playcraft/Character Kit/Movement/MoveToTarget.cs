﻿using UnityEngine;

public class MoveToTarget : IMoveStrategy
{
    Transform self, target;
    IMoveRotate movement;

    public MoveToTarget(IMoveRotate movement)
    {
        this.movement = movement;
        self = movement.GetTransform();
    }

    public void SetTarget(Transform target)
    {
        //Debug.Log(self.gameObject.name + " has set target to " + target.gameObject.name);
        this.target = target;
    }

    public void Step()
    {
        //Debug.Log("Step method called in MoveToTarget");
        if (target == null)
        {
            Debug.LogError("Step method called in MoveToTarget, but no target has been set");
            return;
        }

        movement.RotateStep(target, true);

        if (CheckForMovement())
            movement.MoveForwardStep();
    }

    private bool CheckForMovement()
    {
        if (Vector3.Distance(self.position, target.position) > 0.1f)
            return true;
        else
            return false;
    }
}
