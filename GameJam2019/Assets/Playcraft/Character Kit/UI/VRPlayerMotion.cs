﻿using UnityEngine;

public class VRPlayerMotion : PlayerMotion
{
    [SerializeField] Transform rig, playerEye;
    Vector3 headOffset;
    [SerializeField] CharacterAction snap;

    // Teleport to avatar
    public override void Stop()
    {
        headOffset = rig.transform.position - playerEye.transform.position;
        character.Action.SetState(snap);
        rig.transform.position = character.transform.position + new Vector3(headOffset.x, 0f, headOffset.z);
    }
}
