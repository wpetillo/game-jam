﻿using UnityEngine;

// Converts player input into character commands
public class PlayerMotion : MonoBehaviour
{
    public Character character;
    [SerializeField] protected CharacterAction idle, walkBack, strafeLeft, strafeRight;
    private CharacterAction forwardAction;

    public void OnCommand(MoveCommand command)
    {
        //Debug.Log("On Command: " + command.moveDirection + " " + command.turnDirection);
        SetAction(command);
        character.ReceiveCommand(command);
        command.Reset();
    }

    private void SetAction(MoveCommand command)
    {
        if (command.moveDirection == Vector2.zero && command.turnDirection == 0)
            character.Action.SetState(idle);
        else if (command.moveDirection.y < 0)
            character.Action.SetState(walkBack);
        else if (command.moveDirection.y == 0 && command.moveDirection.x < 0)
            character.Action.SetState(strafeLeft);
        else if (command.moveDirection.y == 0 && command.moveDirection.x > 0)
            character.Action.SetState(strafeRight);
        else
            character.Action.SetState(command.forwardMotionStyle);
    }

    public virtual void Stop() { }
}
