﻿using UnityEngine;

public enum POV { FirstPerson, ThirdPersonForward, ThirdPersonBack }

public class POVcontroller : MonoBehaviour
{
    [SerializeField] POV pointOfView;

    [SerializeField] Transform perspective, firstPerson, thirdPersonForward, thirdPersonBack;
    Transform currentView;

    [SerializeField] GameObject characterModel;

    private void Start()
    {
        SetPOV();
    }

    public void OnPOVChange()
    {
        switch (pointOfView)
        {
            case POV.FirstPerson: pointOfView = POV.ThirdPersonForward; break;
            case POV.ThirdPersonForward: pointOfView = POV.FirstPerson; break;
            case POV.ThirdPersonBack: pointOfView = POV.FirstPerson; break;
        }

        SetPOV();
    }

    public void OnPOVAdjust()
    {
        switch (pointOfView)
        {
            case POV.FirstPerson: break;
            case POV.ThirdPersonForward: pointOfView = POV.ThirdPersonBack; break;
            case POV.ThirdPersonBack: pointOfView = POV.ThirdPersonForward; break;
        }

        SetPOV();
    }

    private void SetPOV()
    {
        switch (pointOfView)
        {
            case POV.FirstPerson:
                currentView = firstPerson;
                characterModel.SetActive(false);
                break;
            case POV.ThirdPersonForward:
                currentView = thirdPersonForward;
                characterModel.SetActive(true);
                break;
            case POV.ThirdPersonBack:
                currentView = thirdPersonBack;
                characterModel.SetActive(true);
                break;
        }

        perspective.transform.localPosition = currentView.localPosition;
        perspective.transform.localRotation = currentView.localRotation; 
    }
}
