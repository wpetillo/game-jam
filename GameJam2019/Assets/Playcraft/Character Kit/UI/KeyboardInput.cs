﻿using UnityEngine;

public class KeyboardInput : MonoBehaviour
{
    [SerializeField] KeyCode[] forward, back, left, right;
    [SerializeField] KeyCode[] turnLeft, turnRight, changePerspective;

    Character character;
    [SerializeField] POVcontroller pov;

    private void Start()
    {
        character = GetComponent<Character>();
    }

    private void Update()
    {
        DetectMoveKeyPress(forward, Vector3.forward);
        DetectMoveKeyPress(back, Vector3.back);
        DetectMoveKeyPress(left, Vector3.left);
        DetectMoveKeyPress(right, Vector3.right);

        DetectTurnKeyPress(turnLeft, -1);
        DetectTurnKeyPress(turnRight, 1);

        DetectPerspectiveKeyPress(changePerspective);
    }

    Vector3 currentDirection;
    private void DetectMoveKeyPress(KeyCode[] keys, Vector3 direction)
    {
        currentDirection += direction;
        currentDirection = currentDirection.normalized;

        //foreach (KeyCode key in keys)
        //    if (Input.GetKey(key))
        //        character.brain.SetState();
    }

    private void DetectTurnKeyPress(KeyCode[] keys, int direction)
    {
        //foreach (KeyCode key in keys)
        //    if (Input.GetKey(key))
         //       movement.RotateStep(direction);
    }

    private void DetectPerspectiveKeyPress(KeyCode[] keys)
    {
        foreach (KeyCode key in keys)
            if (Input.GetKeyDown(key))
                pov.OnPOVChange();
    }
}
