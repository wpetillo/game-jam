﻿using UnityEngine;

public class NPCBrain : IBrain
{
    Character character;
    NPCAbilities abilities;

    GameObject targetTrace;
    bool isPursuing;

    public NPCBrain(Character character)
    {
        this.character = character;
        abilities = character.attributes.Abilities as NPCAbilities;
        character.Action.SetState(abilities.wander);

        targetTrace = new GameObject();
        targetTrace.name = "Target Trace";
        targetTrace.transform.parent = character.transform.parent;
    }

    public void Step()
    {
        if (CanStartAttack())
            character.Action.SetState(abilities.attack);

        if (isPursuing && Vector3.Distance(character.transform.position, targetTrace.transform.position) < 1.5f)
            isPursuing = false;
    }

    private bool CanStartAttack()
    {
        if (character.Action.currentAction == abilities.attack)
            return false;

        return character.Perception.IsInRangeOfTarget(character.attributes.attackRange);
    }

    public void React()
    {
        if (character.Action.currentAction == abilities.idle || 
            character.Action.currentAction == abilities.dead)
            return;

        character.Perception.Look();

        character.Action.SetState(Think());

        if (character.Data.focus.Body != null)
            character.Movement.SetTarget(character.Data.focus.Body.transform); 
        else if (isPursuing)
            character.Movement.SetTarget(targetTrace.transform);
        else
            character.Movement.SetTarget(character.transform);
    }

    public bool Impulse(ActionType action) { return false; }

    private IAction Think()
    {
        if (IsUnderThreat())
        {
            character.Data.panicLevel = character.attributes.maxPanic;
            character.Data.searchFailCount = 0;
            return Flee(0.5f);
        }
        else if (character.Data.panicLevel > 0)
        {
            character.Data.panicLevel--;
            return Flee(0.5f);
        }
        if (character.Action.currentAction == abilities.attack && CanFindTarget()
            && character.Perception.IsInRangeOfTarget(character.attributes.attackRange * 2f))
            return abilities.attack;
        else if (CanFindTarget())
        {
            isPursuing = true;
            return abilities.runTowards;
        }
        else if (isPursuing)
            return abilities.runTowards;

        return abilities.wander;
    }

    // * Merge with IsUnderThreat
    public bool CanFindTarget()
    {
        character.Data.focus.Body = character.Perception.FindClosest(character.transform.position, character.Data.observations.targetsInSight);

        if (character.Data.focus.Body != null)
            targetTrace.transform.position = character.Data.focus.Body.transform.position;

        if (character.Data.focus.Body == null)
            return false;
        else
            return true;
    }

    private bool IsUnderThreat()
    {
        // * Remove reference to Flyweight
        character.Data.focus.Body = character.Perception.FindClosest(character.transform.position, character.Data.observations.threatsInSight);

        if (character.Data.focus.Body == null)
            return false;
        else
            return true;
    }

    private IAction Flee(float evasiveness)
    {
        if (Random.Range(0, evasiveness) < 1)
            return abilities.scatter;
        else
            return abilities.runAway;
    }
}