﻿using UnityEngine;

public class AvatarBrain : IBrain
{
    Character character;

    public AvatarBrain(Character character)
    {
        this.character = character;
    }

    public void React()
    {
        //GetFindFoodStrategy(character).RequestAttack(character);
    }

    public bool Impulse(ActionType action)
    {
        //Debug.Log("Receiving impulse to " + action);
        switch (action)
        {
            //case ActionType.Eat: return ReceiveEatCommand(character);
            default: Debug.LogError("Cannot act on impulse!"); return false;
        }
    }

    private bool ReceiveEatCommand()
    {
        //Debug.Log("Received eat command..." + character.data.attributes.dietStrategy);
        /*if (GetFindFoodStrategy(character).CanEat(character))
        {
            character.actionState.SetState(ActionType.Eat);
            return true;
        }
        else return false;*/
        return false;
    }

    // * Factor into seperate class to share with AnimalBrain
    /*private IGetFood GetFindFoodStrategy(Character character)
    {
        switch (character.data.attributes.dietStrategy)
        {
            case Diet.Carnivore: return Flyweight.instance.findFoodStrategies.carnivore;
            case Diet.Herbivore: return Flyweight.instance.findFoodStrategies.herbivore;
            default: Debug.LogError("Invalid diet type on animal initialization"); return null;
        }
    }*/

    public void Step() { }

    public ActionType OnMealFinished()
    {
        return ActionType.WalkTowards;
    }
}
