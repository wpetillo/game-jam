﻿using UnityEngine;

public class BrainController : IBrain
{
    Character character;
    IBrain npc, avatar;
    public IBrain brain { get; private set; }

    public BrainController(Character character)
    {
        this.character = character;
        SetBrain(character.attributes);
    }

    private void SetBrain(Attributes attributes)
    {
        switch (attributes.characterType)
        {
            case CharacterType.NPC:
                if (npc == null)
                    npc = new NPCBrain(character);
                brain = npc;
                break;
            case CharacterType.Player:
                if (avatar == null)
                    avatar = new AvatarBrain(character);
                brain = avatar;
                break;
            default:
                Debug.LogError("Attempting to set invalid Brain type!");
                break;
        }
    }

    public void Step()
    {
        brain.Step();
    }

    public void React()
    {
        brain.React();
    }
}
