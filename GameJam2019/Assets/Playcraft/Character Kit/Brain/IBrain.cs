﻿public interface IBrain
{
    void Step();
    void React();
}
