﻿using UnityEngine;

// Controls which input configuration is active
public class InputManager : MonoBehaviour
{
    public InputConfiguration[] inputConfigurations;
    [SerializeField] InputConfiguration activeConfiguration;

    private void Update()
    {
        activeConfiguration.Update();
    }
}
