﻿using UnityEngine;

// * As this becomes more generalized, use inheritance to delegate out binding groups
[CreateAssetMenu(fileName = "Input Configuration", menuName = "Playcraft/Input/Input Configuration")]
public class InputConfiguration : ScriptableObject
{
    public KeyBinding[] allBindings;

    public Vector2KeyBinding[] move;
    public IntKeyBinding[] turn;
    public SimpleKeyBinding speedUp, slowDown;
    public SimpleKeyBinding jump;
    public CharacterAction[] forwardMotionStyles;

    [SerializeField] MoveCommandGameEvent moveEvent;
    MoveCommand command;

    public void Update()
    {
        if (command == null)
            command = new MoveCommand(forwardMotionStyles);

        command.moveDirection = Vector2.zero;

        foreach (KeyBinding input in allBindings)
            input.Update();

        foreach (Vector2KeyBinding input in move)
            command.moveDirection += input.currentValue;
        foreach (IntKeyBinding input in turn)
            command.turnDirection += input.currentValue;

        if (speedUp.currentValue == true)
        {
            speedUp.currentValue = false;
            command.SpeedUp();
        }

        if (slowDown.currentValue == true)
        {
            slowDown.currentValue = false;
            command.SlowDown();
        }

        if (jump.currentValue == true)
        {
            jump.currentValue = false;
            command.isJumping = true;
        }

        moveEvent.Raise(command);
    }
}
