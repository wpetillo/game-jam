﻿using UnityEngine;

[CreateAssetMenu(fileName = "Key Binding", menuName = "Playcraft/Input/Key Binding/Vector2")]
public class Vector2KeyBinding : KeyBinding
{
    [SerializeField] Vector2 activeValue;
    [HideInInspector] public Vector2 currentValue;

    protected override void SetValue(bool isPressed)
    {
        if (isPressed) currentValue = activeValue;
        else currentValue = Vector2.zero;
    }
}
