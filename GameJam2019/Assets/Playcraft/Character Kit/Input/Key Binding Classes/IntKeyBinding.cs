﻿using UnityEngine;

[CreateAssetMenu(fileName = "Key Binding", menuName = "Playcraft/Input/Key Binding/Int")]
public class IntKeyBinding : KeyBinding
{
    [SerializeField] int activeValue;
    [HideInInspector] public int currentValue;

    protected override void SetValue(bool isPressed)
    {
        if (isPressed) currentValue = activeValue;
        else currentValue = 0;
    }
}
