﻿using UnityEngine;

[CreateAssetMenu(fileName = "Key Binding", menuName = "Playcraft/Input/Key Binding/Simple")]
public class SimpleKeyBinding : KeyBinding
{
    [HideInInspector] public bool currentValue;

    protected override void OnPressDown()
    {
        currentValue = true;
    }

    protected override void OnPressUp()
    {
        currentValue = false;
    }
}
