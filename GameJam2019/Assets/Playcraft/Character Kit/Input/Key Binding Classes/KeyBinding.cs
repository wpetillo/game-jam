﻿using UnityEngine;

public class KeyBinding : ScriptableObject
{
    [SerializeField] KeyCode[] keys;  

    public void Update()
    {
        foreach (KeyCode key in keys)
        {
            if (Input.GetKeyDown(key))
                OnPressDown();
            else if (Input.GetKeyUp(key))
                OnPressUp();

            SetValue(Input.GetKey(key));
        }
    }

    protected virtual void OnPressDown() { }
    protected virtual void OnPressUp() { }
    protected virtual void SetValue(bool isPressed) { }
}
