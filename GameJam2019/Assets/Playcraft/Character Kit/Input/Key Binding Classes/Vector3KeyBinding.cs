﻿using UnityEngine;

[CreateAssetMenu(fileName = "Key Binding", menuName = "Playcraft/Input/Key Binding/Vector3")]
public class Vector3KeyBinding : KeyBinding
{
    [SerializeField] Vector3 activeValue;
    [HideInInspector] public Vector3 currentValue;

    protected override void OnPressDown()
    {
        currentValue = activeValue;
    }

    protected override void OnPressUp()
    {
        currentValue = Vector3.zero;
    }
}
