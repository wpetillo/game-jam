﻿using UnityEngine;

[CreateAssetMenu(fileName = "Key Binding", menuName = "Playcraft/Input/Key Binding/Simple Trigger")]
public class SimpleTriggerKeyBinding : KeyBinding
{
    [SerializeField] SimpleGameEvent gameEvent;

    protected override void OnPressDown()
    {
        gameEvent.Raise();
    }
}
