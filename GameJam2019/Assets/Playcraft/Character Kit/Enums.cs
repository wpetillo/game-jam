﻿public enum ActionType
{
    Wander,
    Eat,
    RunTowards,
    RunAway,
    Dead,
    Idle,
    WalkTowards,
    Attack,
    Scatter,
    Sprint,
    SnapToTarget,
    Search,
    StrafeLeft,
    StrafeRight
}

public enum AnimationType
{
    Walk,
    WalkBack,
    Run,
    Eat,
    Die,
    Idle,
    Attack,
    Search,
    StrafeLeft,
    StrafeRight,
    Jump
}

public enum MoveType { ToTarget, FromTarget, Random, Still, TrackObject, Free }

public enum Diet { Herbivore, Carnivore, Omnivore }

public enum CharacterType { NPC, Player }

public enum JumpType { Normal, Double, Wall }
