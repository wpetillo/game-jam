﻿using UnityEngine;

public class MusicController : MonoBehaviour
{
    [SerializeField] AudioClip[] musicTracks;
    AudioSource audio;

    private void Awake()
    {
        audio = GetComponent<AudioSource>();
    }

    public void PlaySong(int index)
    {
        if (index >= musicTracks.Length)
        {
            Debug.LogError("Invalid music track ID " + index);
            return;
        }

        audio.clip = musicTracks[index];
        audio.Play();
    }
}
