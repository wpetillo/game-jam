﻿using UnityEngine;
//using RootMotion.FinalIK;
public class Grabber : MonoBehaviour
{
    //public SteamVR_Controller.Device controller
    //{
      //  get
       // {
        //    return SteamVR_Controller.Input((int)trackedObj.index);
     //   }
   // }
  //  private SteamVR_TrackedObject trackedObj;

    private Vector3 grabPos;
    private Vector3 objectPos;
    private Quaternion objectRot;
    private Quaternion grabRot;
  //  public Transform grabObject;
    //public TrackerSetter trackerSetter;
    public Transform rootObject;
    [HideInInspector]
    public bool isGrabbing;     // Are we currently grabbing a collider that is tagged with Climbable
    
    void Start()
    {
      //  trackedObj = transform.parent.GetComponent<SteamVR_TrackedObject>();

    }
    
    void Update()
    {
        //if (trackerSetter.activeIkBody != null)
        {
            if (Input.GetButtonDown("Trigger"))//controller.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
            {
  //              trackerSetter.activeIkBody.GetComponent<VRIK>().solver.locomotion.weight = 0;
                grabPos = transform.position;
                objectPos = rootObject.position;
                grabRot = transform.parent.localRotation;
                objectRot = rootObject.localRotation;
                isGrabbing = true;
            }
            if (isGrabbing)
            {
                if (Input.GetButtonUp("Trigger"))// controller.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
                {
                    isGrabbing = false;
                }
                else
                {
                    Vector3 moveVector = transform.position - grabPos; //3d move vector
      //              trackerSetter.activeIkBody.position = objectPos + moveVector;
                    rootObject.position = objectPos + moveVector;

                    rootObject.localRotation = Quaternion.Euler( transform.parent.localRotation.eulerAngles.x - grabRot.eulerAngles.x + objectRot.eulerAngles.x, rootObject.localRotation.eulerAngles.y, 0 );
                    //trackerSetter.activeIkBody.rotation = transform.rotation;
                }
            }
        }
    }
}