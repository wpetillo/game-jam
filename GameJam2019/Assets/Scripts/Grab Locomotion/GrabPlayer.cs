﻿using UnityEngine;

public class GrabPlayer : MonoBehaviour
{
    public bool activeGrab;
    public GameObject cam;
    private SphereCollider sphereCollider;
    public Vector3 tossMultiplier = Vector3.one;
    private GameObject capsulePos;
    public bool isGrounded;
    private Rigidbody rootRigidbody;

    public bool creativeMode;
    public bool useGravity = true;


    private void Start()
    {
        sphereCollider = GetComponent<SphereCollider>();
        rootRigidbody = GetComponent<Rigidbody>();
    }

    public void ToggleCreativeMode()
    {
        if (creativeMode)
            SetNormalMode();
        else
            SetCreativeMode();
    }

    public void SetCreativeMode()
    {
        creativeMode = true;
        useGravity = false;
        rootRigidbody.useGravity = false;
        rootRigidbody.drag = .2f;
    }

    public void SetNormalMode()
    {
        creativeMode = false;
        useGravity = true;
        rootRigidbody.useGravity = true;
        rootRigidbody.drag = 0;
    }

    private void Update()
    {
        if (capsulePos == null)
        {
            sphereCollider.center = new Vector3(cam.transform.localPosition.x, .15f, cam.transform.localPosition.z);

            if (gameObject.transform.Find("Bip002 Pelvis") != null)       // This is performance horror but works, should be set when player prefab is spawned instead.
            {
                capsulePos = gameObject.transform.Find("Bip002 Pelvis").gameObject;
            }
        }
        else
        {
            sphereCollider.center = new Vector3(transform.InverseTransformPoint(capsulePos.transform.position).x, .15f, transform.InverseTransformPoint(capsulePos.transform.position).z);
        }
        if (creativeMode)
            isGrounded = true;
        else
            isGrounded = Physics.Raycast(transform.TransformPoint(sphereCollider.center + (Vector3.down * .05f)), Vector3.down, .15f);
    }
}
