﻿using UnityEngine;
using System.Collections;

public class GrabLocomotion : MonoBehaviour
{
    public bool isLeftHand;

//    public SteamVR_Controller.Device controller
//    {
//        get
//        {
//            return SteamVR_Controller.Input((int)trackedObj.index);
//        }
//    }
//    private SteamVR_TrackedObject trackedObj;
private Transform trackedObj;
    private Vector3 grabPos;
    private Vector3 offset;
    private Rigidbody rootRigidBody;

    public Transform camera;

    public GrabPlayer player;

    private Collider playerCollider;

    private GrabPlayer grabPlayer;

    [HideInInspector]
    public bool isGrabbing;     // Are we currently grabbing a collider that is tagged with Climbable
    [HideInInspector]
    public bool isClimbing;

    void Start()
    {
  //      trackedObj = transform.parent.GetComponent<SteamVR_TrackedObject>();
        rootRigidBody = player.gameObject.GetComponent<Rigidbody>();
        player = rootRigidBody.GetComponent<GrabPlayer>();
        playerCollider = rootRigidBody.GetComponent<Collider>();
        grabPlayer = FindObjectOfType<GrabPlayer>();
    trackedObj = transform;
        //tempGrab = Instantiate(spawnableGrab, transform.position, Quaternion.identity) as GameObject;
    }

    void OnTriggerStay(Collider col)            // We really shouldn't use OnTriggerStay so at some point fix this
    {
        if (col.gameObject.tag == "Climbable")
        {
            if (Input.GetButtonDown("Trigger"))//controller.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
            {
                StartCoroutine(LongVibration(.05f, 1f)); // Give haptic feedback to the fact the player just grabbed something

                grabPos = col.transform.position;
                //grabbedCol = col;

                offset = transform.position - col.transform.position;

                isGrabbing = true;
                isClimbing = true;

                rootRigidBody.velocity = Vector3.zero;
                rootRigidBody.angularVelocity = Vector3.zero;

                player.activeGrab = isLeftHand;

                rootRigidBody.useGravity = false;
            }
        }
    }
    bool spinFlag = false;
    void Update()
    {
Debug.Log(Input.GetAxis("Vertical"));
        //if (Input.GetAxis(KeyCode.JoystickButton14))//.GetButtonDown("Trigger"))//controller.GetPressDown(SteamVR_Controller.ButtonMask.Trigger) && player.isGrounded)
        {Debug.Log("Test");
            player.activeGrab = isLeftHand;
            grabPos = transform.position;
            //grabbedCol = tempGrab.GetComponent<Collider>();
            isGrabbing = true;
            offset = Vector3.zero;

            rootRigidBody.useGravity = false;
        }

        //Handle Grab Locomotion
        if (player.activeGrab == isLeftHand)
        {
            if (isClimbing)
            {
                if (playerCollider.enabled)
                    playerCollider.enabled = false;
            }
            else
                playerCollider.enabled = true;

            if (isGrabbing)
            {
                if (Input.GetButtonUp("Trigger"))//controller.GetPressUp(SteamVR_Controller.ButtonMask.Trigger))
                {
                    rootRigidBody.useGravity = grabPlayer.useGravity;

                    var origin =  trackedObj.parent;
                    if (!grabPlayer.creativeMode)
                    {
                        rootRigidBody.velocity = Vector3.zero;// new Vector3(origin.TransformVector(-controller.velocity).x * player.tossMultiplier.x, origin.TransformVector(-controller.velocity).y * player.tossMultiplier.y, origin.TransformVector(-controller.velocity).z * player.tossMultiplier.z);
                        rootRigidBody.angularVelocity =Vector3.zero;// new Vector3(origin.TransformVector(-controller.angularVelocity).x * player.tossMultiplier.x, origin.TransformVector(-controller.angularVelocity).y * player.tossMultiplier.y, origin.TransformVector(-controller.angularVelocity).z * player.tossMultiplier.z);
                    }
                    else
                    {
                        rootRigidBody.velocity =Vector3.zero;// new Vector3(origin.TransformVector(-controller.velocity).x, origin.TransformVector(-controller.velocity).y, origin.TransformVector(-controller.velocity).z);
                        rootRigidBody.angularVelocity =Vector3.zero;// new Vector3(origin.TransformVector(-controller.angularVelocity).x, origin.TransformVector(-controller.angularVelocity).y, origin.TransformVector(-controller.angularVelocity).z);
                    }
                    isGrabbing = false;
                    isClimbing = false;
                }
                else
                {
                    if (!player.isGrounded && !isClimbing)
                    {
                        rootRigidBody.useGravity = grabPlayer.useGravity;
                    }
                    else
                    {
                        Vector3 moveVector = transform.position - (grabPos + offset); //3d move vector
                        if (isClimbing)
                        {
                            rootRigidBody.transform.Translate(-moveVector); // Move the playspace to the hand transform minus the difference to the grabbed collider, plus an offset from the colliders origin transform
                        }
                        else
                        {
                            float vecMagnitude = moveVector.magnitude;
                            moveVector = moveVector.normalized * vecMagnitude;
                            if (!grabPlayer.creativeMode)
                                moveVector = new Vector3(moveVector.x, 0, moveVector.z); //flatten vector
                            //rootRigidBody.AddForce
                            rootRigidBody.MovePosition(rootRigidBody.transform.position - moveVector);
                            //rootRigidBody.transform.Translate(-moveVector); // Move the playspace to the hand transform minus the difference to the grabbed collider, plus an offset from the colliders origin transform
                        }
                    }
                }
            }
        }
        else
        {
            isGrabbing = false;
            isClimbing = false;
        }

//        if(controller.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis0)[0] > 0.8f)
//        {
//            if (!spinFlag)
//                rootRigidBody.transform.RotateAround(camera.position, Vector3.up, 90);
//                //rootRigidBody.MoveRotation(rootRigidBody.rotation * Quaternion.Euler(new Vector3(0, 90, 0)));
//            spinFlag = true;
//        }
//        else if (controller.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis0)[0] < -0.8f)
//        {
//            if (!spinFlag)
//                rootRigidBody.transform.RotateAround(camera.position, Vector3.up, -90);
//            //rootRigidBody.MoveRotation(rootRigidBody.rotation * Quaternion.Euler(new Vector3(0, -90, 0)));
//            spinFlag = true;
//        }
//        else
        {
            spinFlag = false;
        }
    }

    IEnumerator LongVibration(float length, float strength)
    {
        for (float i = 0; i < length; i += Time.deltaTime)
        {
            //TODO:Fix this
         //   controller.TriggerHapticPulse((ushort)Mathf.Lerp(0, 3999, strength));
            yield return null;
        }
    }
}