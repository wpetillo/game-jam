﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class FadeSequence : SequenceBase
{
    [SerializeField]
    [Range(0,10)]
    private float _duration = 1;
    private bool _isFading;

    public virtual bool IsDone()
    {
        if (_waitForEnd && _isFading)
            return false;
        return true;
    }

    public virtual bool StartSequence()
    {
        if (_isFading)
            return false;

        _isFading = true;
        StartCoroutine(FadingSequence());
        return true;
    }

    private IEnumerator FadingSequence()
    {
        SteamVR_Fade.Start(Color.clear, 0);
        SteamVR_Fade.Start(Color.black, _duration);

        yield return new WaitForSeconds(_duration);

        SteamVR_Fade.Start(Color.clear, _duration);

        _isFading = false;
    }
}
