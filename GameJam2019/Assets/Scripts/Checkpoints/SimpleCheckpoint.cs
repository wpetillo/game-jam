﻿using UnityEngine;

public class SimpleCheckpoint : MonoBehaviour
{
    public LayerMaskVariable LayerMaskRef;

    public void UnlockGate(LayerMask newMask)
    {
        LayerMaskRef.RuntimeValue = newMask;
    }
}
