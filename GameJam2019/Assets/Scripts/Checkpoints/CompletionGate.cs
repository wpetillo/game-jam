﻿using System.Collections;
using UnityEngine;

public class CompletionGate : MonoBehaviour
{
    [SerializeField] GateType gateType;
    ICheckpoint gate;

    [SerializeField] GameObject player;
    Collider playerHead;
    PlayerDeath death;

    AudioSource audio;
    [SerializeField] AudioClip successClip, failClip;
    [SerializeField] MusicController music;   
    [SerializeField] GameObject gateVisual;
    [SerializeField] int levelToUnlock;
    bool isUnlocked;
    bool hasPlayedSuccessClip = false;
    bool hasPlayedFailClip = false;

    public LayerMaskVariable LayerMaskRef;
    private LayerMask NewLayersMask;

    private void Awake()
    {
        playerHead = player.GetComponent<Collider>();
        death = player.GetComponent<PlayerDeath>();
        audio = GetComponent<AudioSource>();

        switch (gateType)
        {
            case GateType.Checkpoint: gate = new CheckpointGate(); break;
            case GateType.Final: gate = new FinalGate(); break;
            default: Debug.LogError("Invalid gate type on initialization"); break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Entering trigger for " + gameObject.name + " " + isUnlocked);
        if (other != playerHead)
            return;

        audio.clip = isUnlocked ? successClip : failClip;
        
        if (isUnlocked && !hasPlayedSuccessClip)
            AllowPassage();
        else if (!hasPlayedFailClip)
            RefusePassage();
    }

    public void UnlockGate(LayerMask newMask)
    {
        Debug.Log("Unlocking gate " + gameObject.name);
        isUnlocked = true;

        NewLayersMask = newMask;
    }

    private void AllowPassage()
    {
        death.checkpoint = levelToUnlock;
        LayerMaskRef.RuntimeValue = NewLayersMask;

        gateVisual.SetActive(false);
        hasPlayedSuccessClip = true;
        music.PlaySong(levelToUnlock);
        gate.EnterGate(successClip);
        audio.Play();
    }

    private void RefusePassage()
    {
        hasPlayedFailClip = true;
        audio.Play();
    }
}
