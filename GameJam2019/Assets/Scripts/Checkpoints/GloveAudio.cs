﻿using UnityEngine;

public class GloveAudio : MonoBehaviour
{
    AudioSource audio;
    [SerializeField] AudioClip firstFound, secondFound;
    [SerializeField] GloveAudio otherGlove;
    public bool isSecondGlove = false;

    private void OnEnable()
    {
        audio = GetComponent<AudioSource>();
        audio.clip = isSecondGlove ? secondFound : firstFound;
        audio.Play();
        otherGlove.isSecondGlove = true;
    }
}
