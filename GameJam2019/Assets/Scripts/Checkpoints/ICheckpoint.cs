﻿using UnityEngine;

public enum GateType { Checkpoint, Final }

public interface ICheckpoint
{
    void EnterGate(AudioClip clip);
}