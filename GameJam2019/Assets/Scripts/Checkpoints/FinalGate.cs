﻿using System;
using UnityEngine;

public class FinalGate : ICheckpoint
{
    bool quitFlag;
    Action Quit;

    public FinalGate()
    {
        quitFlag = false;
        Quit = QuitGame;
    }

    public void EnterGate(AudioClip clip)
    {
        if (quitFlag)
            return;

        quitFlag = true;
        MonoSim.instance.SimInvoke(Quit, clip.length);
    }

    private void QuitGame()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #else
        Application.Quit();
        #endif
    }
}

public class CheckpointGate : ICheckpoint
{
    public void EnterGate(AudioClip clip)
    {

    }
}