﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve;

public class EnterColliderHaptics : MonoBehaviour
{
    private Hand _hand;
    private bool _isShaking;

    private void Start()
    {
        _hand = GetComponent<Hand>();
    }

    private void OnTriggerEnter(Collider other)
    {
        var haptic = other.gameObject.GetComponent<HapticObject>();
        if (haptic == null)
            return;

        if (haptic.OnEnter)
        {
            StartCoroutine(Shake(haptic.pulse, haptic.period, haptic.duration));
        }
    }

    private void OnTriggerStay(Collider other)
    {
        var haptic = other.gameObject.GetComponent<HapticObject>();
        if (haptic == null)
            return;

        //Debug.Log("Collision with haptic object " + haptic.OnStay + " " + _isShaking);
        if (haptic.OnStay)
        {
            if (!_isShaking)
                StartCoroutine(Shake(haptic.pulse, haptic.period, haptic.duration));
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var haptic = other.gameObject.GetComponent<HapticObject>();
        if (haptic == null)
            return;

        if (haptic.OnExit)
        {
            StartCoroutine(Shake(haptic.pulse, haptic.period, haptic.duration));
        }
    }

    private void OnInvoke(GameObject other)
    {
        var haptic = other.GetComponent<HapticObject>();
        if (haptic == null)
            return;

        if (haptic.OnInvoke)
        {
            StartCoroutine(Shake(haptic.pulse, haptic.period, haptic.duration));
        }
    }

    private IEnumerator Shake(float pulse, float period, float duration)
    {
        //Debug.Log("Shake coroutine reached..." + _isShaking + " " + (_hand == null));
        if (!_isShaking && _hand != null)
        {
            _isShaking = true;

            var wait = new WaitForSeconds(period);
            float timer = 0;

            //Debug.Log("Shake Start");
            while (timer <= duration)
            {
                _hand.hapticAction.Execute(0, duration, period, pulse, _hand.thisHand);
                yield return period;
                timer += period;
            }
            //Debug.Log("Shake end");
            _isShaking = false;
        }
    }
}