﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HapticObject : MonoBehaviour
{
    public bool OnEnter;
    public bool OnStay;
    public bool OnExit;
    public bool OnInvoke;

    public float pulse = 1;
    public float period = 0.05f;
    public float duration = 0.5f;
}
