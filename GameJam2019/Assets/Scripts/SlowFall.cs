﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowFall : MonoBehaviour
{
    [SerializeField] float maxSpeed;
    Rigidbody rigRB;

    private void Start()
    {
        rigRB = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (rigRB.velocity.magnitude > maxSpeed)
            rigRB.velocity = rigRB.velocity.normalized * maxSpeed;
    }
}
