﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneShotAudioClip : SequenceBase
{
    public AudioSource Source;
    public AudioClip clip;

    private bool _isPlaying;

    public override bool IsDone()
    {
        if (_waitForEnd && _isPlaying)
            return false;

        return true;
    }

    public override bool StartSequence()
    {
        if (_isPlaying)
            return false;

        _isPlaying = true;
        StartCoroutine(PlayingSequence());
        return true;
    }

    private IEnumerator PlayingSequence()
    {
        if (Source != null)
        {
            Source.clip = clip;
            Source.Play();

            if (clip != null)
                yield return new WaitForSeconds(clip.length);

            _isPlaying = false;
        }

    }
}
