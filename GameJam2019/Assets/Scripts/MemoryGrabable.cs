﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemoryGrabable : MonoBehaviour, IGrabbable
{
    [SerializeField] 
    ClimbAreaLock areaLock;
    [Header("Optional")]
    [SerializeField]
    private ParticleSystem _particleEffect;
    public MeshRenderer[] BubbleRenderers;
    [SerializeField]
    GameObject narrationToActivate;

    private void Start()
    {
        _particleEffect = GetComponentInChildren<ParticleSystem>();
    }

    public GameObject GetObject()
    {
        return gameObject;
    }

    public void OnGrab(Transform hand)
    {
        areaLock.OnEventRaised(gameObject);
        StartCoroutine(EndSequence());
    }

    public void OnRelease(Transform hand, Vector3 velocity, Vector3 angularVelocity)
    {
    }

    private IEnumerator EndSequence()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio?.Play();

        //Debug.Log("here--------");
        for (int i = 0; i < BubbleRenderers.Length; i++)
        {
            BubbleRenderers[i].enabled = false;
        }

        if (_particleEffect != null)
        {
            _particleEffect.Stop();
            _particleEffect.Play();
            _particleEffect.enableEmission = true;
            //Debug.Log("here-------1");
            yield return new WaitForSeconds(audio.clip.length);
        }

        narrationToActivate.SetActive(true);
        gameObject.SetActive(false);

        for (int i = 0; i < BubbleRenderers.Length; i++)
        {
            BubbleRenderers[i].enabled = true;
        }
    }
}
