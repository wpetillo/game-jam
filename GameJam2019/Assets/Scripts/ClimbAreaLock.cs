﻿using UnityEngine;
using UnityEngine.Events;

public class ClimbAreaLock : GameEventListener
{
    [SerializeField]
    private GameObject[] _memories;
    public LayerMask NewLayersMask;
    private int _memoryCount;

    public KeyCode hotKeyAdvance;
    public int debugCount;
    public LayerEvent unlockEvent;

    public override void OnEventRaised(GameObject value) 
    {
        //Debug.Log("object = " + value.name);
        for (int i = 0; i < _memories.Length; i++)
        {
            if (value == _memories[i])
                _memoryCount++;
        }
        //Debug.Log("Acquired memory:" + value.name);

        if (_memoryCount == _memories.Length)
        {
            //Debug.Log("New level reached:" + LayerMaskRef.RuntimeValue);
            _memoryCount = 0;   // * Hack fix: method should not be called for wrong gate
            unlockEvent.Invoke(NewLayersMask);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(hotKeyAdvance))
        {
            Debug.Log("Pressed advance hotkey");
            unlockEvent.Invoke(NewLayersMask);
        }
    }
}
