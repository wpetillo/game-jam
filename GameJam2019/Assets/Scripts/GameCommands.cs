﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCommands : MonoBehaviour
{
    [SerializeField] KeyCode quitGame;

    private void Update()
    {
        if (Input.GetKey(quitGame))
            QuitGame();
    }

    private void QuitGame()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #else
        Application.Quit();
        #endif
    }
}
