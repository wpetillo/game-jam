﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ClimbHand : Hand
{
    private LayerUtility layerUtil = new LayerUtility();
    [SerializeField] LayerMaskVariable grabbableLayers;

    Rigidbody rigRB;
    [SerializeField] Transform rig;
    Collider head;
    [SerializeField] ClimbHand otherHand;
    public bool isGrabbing;
    public bool isTouchingGrabbable;

    [SerializeField] Renderer handRenderer;
    [SerializeField] Color inactiveColor, activeColor;

    private Vector3 moveCur, movePrev, moveDelta;
    private static int CurrentHandValue;

    [SerializeField] bool isThrowSelfUnlocked;
    [SerializeField] float throwStrength;

    public SteamVR_Action_Boolean teleport;
    [SerializeField] Transform eyes;
    [SerializeField] float hopDistance;
    [SerializeField] float hopCooldownTime;
    bool hopIsCoolingDown;

    protected override void Awake()
    {
        base.Awake();

        rigRB = rig.GetComponent<Rigidbody>();
        head = rig.GetComponent<Collider>();
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        if (teleport != null)
            teleport.AddOnChangeListener(Hop, thisHand);
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        if (teleport != null)
            teleport.RemoveOnChangeListener(Hop, thisHand);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (layerUtil.IsIndexInBinary(other.gameObject.layer, grabbableLayers.RuntimeValue))
        {
            isTouchingGrabbable = true;
            handRenderer.material.SetColor("_EmissionColor", activeColor);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (layerUtil.IsIndexInBinary(other.gameObject.layer, grabbableLayers.RuntimeValue))
        {
            isTouchingGrabbable = false;
            handRenderer.material.SetColor("_EmissionColor", inactiveColor);
        }
    }

    protected override void Grab()
    {
        base.Grab();

        RequestPull();
    }

    public void RequestPull()
    {
        if (isTouchingGrabbable)
            BeginPull();
    }

    private void Hop(SteamVR_Action_In actionIn)
    {
        if (teleport.GetStateDown(thisHand))
        {
            Vector3 direction = eyes.forward;
            direction = new Vector3(direction.x, 0, direction.z);
            rig.Translate(direction * hopDistance, Space.World);

            hopIsCoolingDown = true;
            Invoke("EndHopCooldown", hopCooldownTime);
        }
    }

    private void EndHopCooldown()
    {
        hopIsCoolingDown = false;
    }

    private void BeginPull()
    {
        //Debug.Log("Beginning pull...");
        movePrev = Vector3.zero;

        isGrabbing = true;
        otherHand.isGrabbing = false;
        head.enabled = false;
        rigRB.useGravity = false;
        rigRB.velocity = Vector3.zero;
    }

    private void Update()
    {
        if (isGrabbing && !otherHand.isGrabbing)
            MoveRig();
    }

    private void MoveRig()
    {
        moveCur = transform.position;

        // Initialize on starting frame
        if (movePrev == Vector3.zero)
            movePrev = moveCur;
        else
        {
            moveDelta = movePrev - moveCur;
            rig.position += moveDelta;
            moveCur += moveDelta;
            movePrev = moveCur;
        }
    }

    protected override void Release(Vector3 velocity, Vector3 angularVelocity)
    {
        base.Release(velocity, angularVelocity);

        isGrabbing = false;

        if (otherHand.GetTriggerPress() && otherHand.isTouchingGrabbable)
            otherHand.BeginPull();
        else
        {
            head.enabled = true;
            rigRB.useGravity = true;
            ThrowSelf();
        }
    }

    public bool GetTriggerPress()
    {
        return grabPinch.GetState(thisHand);
    }

    private void ThrowSelf()
    {
        rigRB.velocity = (moveDelta / Time.deltaTime) * throwStrength;
    }
}