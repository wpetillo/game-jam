﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRespawn : MonoBehaviour
{
    [SerializeField] Transform rig;
    [SerializeField] KeyCode[] respawnHotkeys;

    private void Update()
    {
        for (int i = 0; i < respawnHotkeys.Length; i++)
            if (Input.GetKeyDown(respawnHotkeys[i]))
                MoveToRespawnLocation(i);
    }

    public void MoveToRespawnLocation(int locationId)
    {
        if (locationId >= transform.childCount)
        {
            Debug.LogError("Cannot respawn at location " + locationId);
            return;
        }

        rig.position = transform.GetChild(locationId).position;
    }
}
