﻿using UnityEngine;

public class PlayerBody : MonoBehaviour
{
    // Utilities
    LayerUtility layers = new LayerUtility();
    [SerializeField] LayerMask standLayer;

    // Dependencies
    [SerializeField] Transform camera;
    [SerializeField] Transform rig;
    [SerializeField] ClimbHand leftHand, rightHand;
    Collider feet;

    // Cached variables
    Ray ray;
    RaycastHit hit;
    float height, headToGround;
    bool isGrabbing;

    void Awake()
    {
        feet = GetComponent<Collider>();
    }

    void Update()
    {
        this.isGrabbing = (leftHand.isGrabbing || rightHand.isGrabbing);

        if (isGrabbing)
        {
            feet.enabled = false;
            return;
        }

        transform.localPosition = new Vector3(camera.localPosition.x, 0f, camera.localPosition.z);  
        height = camera.localPosition.y;

        if (height < .125f)
            return;

        ray = new Ray(camera.position, Vector3.down);
        //Debug.DrawRay(camera.position, Vector3.down * height, Color.red, .05f);

        if (Physics.Raycast(ray, out hit, height * 1.5f) && 
            layers.IsIndexInBinary(hit.collider.gameObject.layer, standLayer))
        {
            feet.enabled = true;
            headToGround = Vector3.Distance(hit.point, camera.position);
            rig.position += Vector3.up * (height - headToGround);
        }
    }
}
