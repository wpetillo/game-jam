﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
    [SerializeField] PlayerRespawn respawner;
    Rigidbody rb;
    public int checkpoint;
    AudioSource audio;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        audio = GetComponent<AudioSource>();
    }

    // TBD: get location ID based on data stored on collider
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("KillZone"))
        {
            respawner.MoveToRespawnLocation(checkpoint);
            audio.Play();

            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }
    }
}
