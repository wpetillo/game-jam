// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "forestfog"
{
	Properties
	{
		_scale("scale", Float) = 0.5
		_Pow("Pow", Float) = 1
		_Sky("Sky", Color) = (0,0,0,0)
		_near("near", Color) = (1,1,1,0)
		_heightscale("heightscale", Float) = 1
		_floorscale("floorscale", Float) = 1
		_HeightOffset("Height Offset", Float) = 0
		_flooroffest("flooroffest", Float) = 0
		_Skyoffset("Skyoffset", Float) = 0.8
		_Opacity("Opacity", Range( 0 , 1)) = 1
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Off
		ZWrite Off
		ZTest Always
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float3 viewDir;
			float4 screenPos;
		};

		uniform float4 _Sky;
		uniform float4 _near;
		uniform float _Skyoffset;
		uniform sampler2D _CameraDepthTexture;
		uniform float _scale;
		uniform float _Pow;
		uniform float _heightscale;
		uniform float _HeightOffset;
		uniform float _floorscale;
		uniform float _flooroffest;
		uniform float _Opacity;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float dotResult16 = dot( i.viewDir , float3(0,1,0) );
			float4 lerpResult9 = lerp( _Sky , _near , ( dotResult16 + _Skyoffset ));
			o.Emission = lerpResult9.rgb;
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float4 tex2DNode36_g1 = tex2D( _CameraDepthTexture, ase_screenPosNorm.xy );
			#ifdef UNITY_REVERSED_Z
				float staticSwitch38_g1 = ( 1.0 - tex2DNode36_g1.r );
			#else
				float staticSwitch38_g1 = tex2DNode36_g1.r;
			#endif
			float3 appendResult39_g1 = (float3(ase_screenPosNorm.x , ase_screenPosNorm.y , staticSwitch38_g1));
			float4 appendResult42_g1 = (float4((appendResult39_g1*2.0 + -1.0) , 1.0));
			float4 temp_output_43_0_g1 = mul( unity_CameraInvProjection, appendResult42_g1 );
			float4 appendResult49_g1 = (float4(( ( (temp_output_43_0_g1).xyz / (temp_output_43_0_g1).w ) * float3( 1,1,-1 ) ) , 1.0));
			float4 temp_output_19_0 = mul( unity_CameraToWorld, appendResult49_g1 );
			o.Alpha = ( saturate( ( saturate( pow( ( distance( temp_output_19_0 , float4( _WorldSpaceCameraPos , 0.0 ) ) * _scale ) , _Pow ) ) + saturate( ( ( temp_output_19_0.y * _heightscale ) + _HeightOffset ) ) + ( 1.0 - saturate( ( ( temp_output_19_0.y * _floorscale ) + _flooroffest ) ) ) ) ) * _Opacity );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD1;
				float4 screenPos : TEXCOORD2;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				o.screenPos = ComputeScreenPos( o.pos );
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.viewDir = worldViewDir;
				surfIN.screenPos = IN.screenPos;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16200
232;72.57143;1253;1082;1067.057;254.046;1.313052;True;False
Node;AmplifyShaderEditor.FunctionNode;19;-654.9466,404.3354;Float;False;Reconstruct World Position From Depth;1;;1;e7094bcbcc80eb140b2a3dbe6a861de8;0;0;1;FLOAT4;0
Node;AmplifyShaderEditor.BreakToComponentsNode;20;34.32774,507.6797;Float;False;FLOAT4;1;0;FLOAT4;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RangedFloatNode;38;15.20056,1043.969;Float;False;Property;_floorscale;floorscale;8;0;Create;True;0;0;False;0;1;0.52;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceCameraPos;40;-608.4455,513.0043;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;22;72.01252,678.8163;Float;False;Property;_heightscale;heightscale;7;0;Create;True;0;0;False;0;1;0.07;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;8;-93.59021,177.1722;Float;False;Property;_scale;scale;0;0;Create;True;0;0;False;0;0.5;0.01;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;42;-253.4455,604.0043;Float;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;37;229.8589,1041.093;Float;False;Property;_flooroffest;flooroffest;10;0;Create;True;0;0;False;0;0;-0.36;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;36;270.1155,915.7096;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;24;286.6708,675.9392;Float;False;Property;_HeightOffset;Height Offset;9;0;Create;True;0;0;False;0;0;-5.63;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;13;89.45935,275.0108;Float;False;Property;_Pow;Pow;4;0;Create;True;0;0;False;0;1;1.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;76.51614,91.546;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;326.9275,550.5562;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;35;448.3896,840.0563;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;12;245.657,144.4154;Float;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;23;505.2015,474.9029;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;34;552.1711,688.1996;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;39;757.1285,605.0478;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;26;608.9831,323.0462;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;18;237.8032,-80.28303;Float;False;Constant;_Vector0;Vector 0;5;0;Create;True;0;0;False;0;0,1,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;17;278.3191,-343.6371;Float;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SaturateNode;25;433.2547,152.7081;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;27;723.5718,148.8722;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;29;564.5175,-383.7413;Float;False;Property;_Skyoffset;Skyoffset;11;0;Create;True;0;0;False;0;0.8;0.7;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;16;489.7621,-260.0728;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;30;856.5138,224.9985;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;28;661.5175,-204.7413;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;15;36.87376,-193.0668;Float;False;Property;_near;near;6;0;Create;True;0;0;False;0;1,1,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;14;-48.44535,-383.661;Float;False;Property;_Sky;Sky;5;0;Create;True;0;0;False;0;0,0,0,0;0.2636844,0.2078431,0.3411765,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;33;671.5048,400.733;Float;False;Property;_Opacity;Opacity;12;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;6;-501.0046,109.6424;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenDepthNode;4;-391.6963,-44.60292;Float;False;0;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;5;-604.5626,191.2413;Float;False;1;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;32;987.2766,312.6863;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;9;488.8577,-127.846;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;1,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;7;1174.858,-27.09808;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;forestfog;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;Off;2;False;-1;7;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Transparent;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;3;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;20;0;19;0
WireConnection;42;0;19;0
WireConnection;42;1;40;0
WireConnection;36;0;20;1
WireConnection;36;1;38;0
WireConnection;10;0;42;0
WireConnection;10;1;8;0
WireConnection;21;0;20;1
WireConnection;21;1;22;0
WireConnection;35;0;36;0
WireConnection;35;1;37;0
WireConnection;12;0;10;0
WireConnection;12;1;13;0
WireConnection;23;0;21;0
WireConnection;23;1;24;0
WireConnection;34;0;35;0
WireConnection;39;0;34;0
WireConnection;26;0;23;0
WireConnection;25;0;12;0
WireConnection;27;0;25;0
WireConnection;27;1;26;0
WireConnection;27;2;39;0
WireConnection;16;0;17;0
WireConnection;16;1;18;0
WireConnection;30;0;27;0
WireConnection;28;0;16;0
WireConnection;28;1;29;0
WireConnection;6;0;4;0
WireConnection;6;1;5;4
WireConnection;32;0;30;0
WireConnection;32;1;33;0
WireConnection;9;0;14;0
WireConnection;9;1;15;0
WireConnection;9;2;28;0
WireConnection;7;2;9;0
WireConnection;7;9;32;0
ASEEND*/
//CHKSM=0DBF1058C0066D3126FDC9956EC917448A6EF8B1