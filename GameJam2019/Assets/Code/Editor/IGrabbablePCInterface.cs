﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(SimpleGameEventProxyIGrabbable))]
public class IGrabbablePCInterface : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SimpleGameEventProxyIGrabbable myScript = (SimpleGameEventProxyIGrabbable)target;
        if(GUILayout.Button("GrabAndLetGo"))
        {
            myScript.OnGrab(null);
        }
    }
}