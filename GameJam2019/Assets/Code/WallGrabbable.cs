﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallGrabbable : MonoBehaviour, IGrabbable
{
    public GameObject GetObject()
    {
        return gameObject;
    }

    public void OnGrab(Transform hand)
    {

    }

    public void OnRelease(Transform hand, Vector3 velocity, Vector3 angularVelocity)
    {

    }
}
