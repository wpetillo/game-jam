﻿using System.Collections;
using UnityEngine;

// TODO: make public constructer.
public class ShakeInDirection : SequenceBase
{
    [SerializeField]
    private bool _useSin;
    [SerializeField]
    private bool _inversed;
    // Don't assign to.
    [HideInNormalInspector]
    public bool IsShaking;

    // The direction The object will shake.
    [SerializeField]
    private Vector3 _direction;

    // The duration of the shake.
    [SerializeField]
    private float _duration;

    // The number of shakes per second.
    [SerializeField]
    [Range(0, 100)]
    private float _period;
    [SerializeField]
    private float _periodIncRate;

    // The distance travled in a shake.
    [SerializeField]
    [Range(0, 100)]
    private float _amplitude;
    [SerializeField]
    private float _amplitudeIncRate;

    private bool _isDone;

    public override bool IsDone()
    {
        if (!_waitForEnd || _isDone)
            return true;

        return false;
    }

    public override bool StartSequence()
    {
        if (IsShaking)
            return false;

        _isDone = false;
        IsShaking = true;
        StartCoroutine(Shake());
        return true;
    }
    
    private IEnumerator Shake()
    {
        float timer = 0;
        // So if we start on a part of the wave not at 0 there won't be a jump.
        float lastSin = _useSin ? 0 : _amplitude;
        lastSin = _inversed ? -lastSin : lastSin;

        var wait = new WaitForFixedUpdate();

        while (timer <= _duration - 0.0001f)
        {
            float Wave = 0;

            // Makes the current wave based on sin/cos.
            if (_useSin)
                Wave = (_inversed ? -1 : 1) * Mathf.Sin(timer * Mathf.PI * (_period + (timer * _periodIncRate))) * (_amplitude + (timer * _amplitudeIncRate));
            else
                Wave = (_inversed ? -1 : 1) * Mathf.Cos(timer * Mathf.PI * (_period + (timer * _periodIncRate))) * (_amplitude + (timer * _amplitudeIncRate));

            float sinDelta = Wave - lastSin;
            lastSin = Wave;

            transform.position += _direction * sinDelta;

            yield return wait;

            // Updates the timer, and makes sure we haven't gone past the alowed time.
            timer = Mathf.Clamp(timer + Time.fixedDeltaTime, 0, _duration);

        }

        IsShaking = false;
        _isDone = true;
    }

#if UNITY_EDITOR
    public void OnValidate()
    {
        // Makes sure we are only shaking in one direction.
        if (!(_direction.x == 1 ^ _direction.y == 1 ^ _direction.z == 1))
        {
            _direction = Vector3.zero;
        }
    }
#endif
}
