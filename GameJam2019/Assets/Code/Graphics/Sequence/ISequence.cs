﻿public interface ISequence
{
    bool StartSequence();
    bool IsDone();
}
