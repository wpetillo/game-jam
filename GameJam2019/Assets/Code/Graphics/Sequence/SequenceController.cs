﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SequenceController : MonoBehaviour
{
    [SerializeField]
    private SequenceBase[] Sequences;

    private bool _isPlaying;

    /// <summary>
    /// The function to call to start the sequence.
    /// </summary>
    public void StartSequences()
    {
        if (_isPlaying)
            return;

        _isPlaying = true;
        Debug.Log("Started");
        StartCoroutine(ControllingSequence());
    }

    /// <summary>
    /// The main sequence that waits/plays other sequences;
    /// </summary>
    private IEnumerator ControllingSequence()
    {
        for (int i = 0; i < Sequences.Length; i++)
        {
            Debug.Log("Sequence = " + i);
            // If sequence was not started go to next.
            if (!Sequences[i].StartSequence())
                continue;

            // If we are suposed to wait, then wait.
            if (!Sequences[i].IsDone())
                yield return new WaitUntil(() => Sequences[i].IsDone());
        }

        _isPlaying = false;

        // Move to game scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
