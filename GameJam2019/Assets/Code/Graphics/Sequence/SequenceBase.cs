﻿using UnityEngine;

public class SequenceBase : MonoBehaviour, ISequence
{
    [SerializeField]
    protected bool _waitForEnd = true;
    public virtual bool IsDone()
    {
        if (_waitForEnd)
            return false;
        return true;
    }

    public virtual bool StartSequence()
    {
        return false;
    }

}
