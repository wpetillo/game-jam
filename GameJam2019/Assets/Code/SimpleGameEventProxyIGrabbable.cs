﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleGameEventProxyIGrabbable : MonoBehaviour, IGrabbable
{
    [SerializeField]
    private bool _grabRepeating;

    [SerializeField]
    private SimpleGameEvent _onGrabEvent;
    [SerializeField]
    private SimpleGameEvent _onReleaseEvent;
    private bool _grabbing;

    public virtual GameObject GetObject()
    {
        return gameObject;
    }

    public virtual void OnGrab(Transform hand)
    {
        _grabbing = true;

        // Prevents it from being raised multple times on first frame.
        if (!_grabRepeating)
            _onGrabEvent.Raise();
    }  

    public virtual void OnRelease(Transform hand, Vector3 velocity, Vector3 angularVelocity)
    {
        _grabbing = false;

        if (_onReleaseEvent != null)
            _onReleaseEvent.Raise();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (_grabbing && _grabRepeating)
            _onGrabEvent.Raise();
    }
}
